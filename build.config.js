/**
 * This file/module contains all configuration for the build process.
 */
module.exports = {
  /**
   * The `build_dir` folder is where our projects are compiled during
   * development and the `compile_dir` folder is where our app resides once it's
   * completely built.
   */
  dev_dir: 'client/build',
  build_dir: 'client/public',
  compile_dir: 'client/bin',

  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `build/`). These file paths are used in the configuration of
   * build tasks. `js` is all project javascript, less tests. `ctpl` contains
   * our reusable components' (`build/common`) template HTML files, while
   * `atpl` contains the same, but for our app's code. `html` is just our
   * main HTML file, `less` is our main stylesheet, and `unit` contains our
   * app's unit tests.
   */
  app_files: {
    js: [ 'client/build/**/*.js', '!client/build/**/*.spec.js', '!client/build/assets/**/*.js' ],
    jsunit: [ 'client/build/**/*.spec.js' ],

    atpl: [ 'client/build/app/**/*.tpl.html' ],
    ctpl: [ 'client/build/common/**/*.tpl.html' ],
    appbase: 'client/build/app',
    commonbase: 'client/build/common',

    assets: 'client/build/assets/**',

    html: [ 'client/build/index.html' ],
    scss_dir: 'client/build/scss',
    scss: ['client/build/scss/style.scss', 'client/build/scss/ie.scss']
  },

  /**
   * This is a collection of files used during testing only.
   */
  test_files: {
    js: [
      'vendor/bower_components/angular/angular.js',
      'vendor/bower_components/angular-mocks/angular-mocks.js',
      'vendor/bower_components/angular-ui-router/release/angular-ui-router.min.js',
      'client/build/**/*.spec.js'
    ]
  },

  /**
   * This is the same as `app_files`, except it contains patterns that
   * reference vendor code (`vendor/`) that we need to place into the build
   * process somewhere. While the `app_files` property ensures all
   * standardized files are collected for compilation, it is the user's job
   * to ensure non-standardized (i.e. vendor-related) files are handled
   * appropriately in `vendor_files.js`.
   *
   * The `vendor_files.js` property holds files to be automatically
   * concatenated and minified with our project source files.
   *
   * The `vendor_files.css` property holds any CSS files to be automatically
   * included in our app.
   *
   * The `vendor_files.assets` property holds any assets to be copied along
   * with our app's assets. This structure is flattened, so it is not
   * recommended that you use wildcards.
   */
  vendor_files: {
    js: [
      'vendor/bower_components/jquery/dist/jquery.min.js',
      'vendor/bower_components/jcrop/js/jquery.Jcrop.min.js',
      'vendor/bower_components/caman/dist/caman.full.js',
      // 'vendor/bower_components/fastclick/lib/fastclick.js',
      'vendor/bower_components/moment/min/moment.min.js',
      'vendor/bower_components/angular/angular.js',
      'vendor/bower_components/angular-animate/angular-animate.js',
      'vendor/bower_components/angular-resource/angular-resource.js',
      'vendor/bower_components/angular-sanitize/angular-sanitize.js',
      'vendor/bower_components/angular-moment/angular-moment.min.js',
      // 'vendor/bower_components/angular-touch/angular-touch.js',
      'vendor/bower_components/angular-ui-router/release/angular-ui-router.min.js',
      'vendor/bower_components/ng-file-upload/angular-file-upload-shim.min.js',
      'vendor/bower_components/ng-file-upload/angular-file-upload.min.js',
      'vendor/bower_components/angular-locker/dist/angular-locker.js',
      'vendor/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.min.js',
      // 'vendor/bower_components/angular-modal-service/dst/angular-modal-service.js'
      'node_modules/lodash/dist/lodash.min.js',
      'vendor/bower_components/angular-bootstrap/ui-bootstrap-tpls.js'
    ],
    css: [
      'vendor/bower_components/jcrop/css/jquery.Jcrop.min.css'
    ],
    assets: [
      // 'vendor/bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*'
    ]
  },
};
