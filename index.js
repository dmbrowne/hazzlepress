#!/usr/bin/env node
var debug  = require('debug')('hazzlepress');
var appjs  = require('./app');                // The meat and bones of the server

appjs.set('port', process.env.PORT || 3000);

var server = appjs.listen(appjs.get('port'), function() {
	console.info("App listening on port: " + appjs.get('port') );
	debug('Express server listening on port ' + server.address().port);
});
