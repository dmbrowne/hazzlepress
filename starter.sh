#!/bin/sh

if [ $(ps -e -o uid,cmd | grep $UID | grep node | grep -v grep | wc -l | tr -s "\n") -eq 0 ]
then
        export PATH=/usr/local/bin:$PATH
        forever start --sourceDir /home/dbrowne/Sites/hazzlepress index.js >> /home/dbrowne/Sites/hazzlepress.txt 2>&1
fi
