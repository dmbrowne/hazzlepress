'use strict';

angular.module('hazzlepress.search.services', [])

.factory('SearchQuery', [function(){
	// this.searchTerms = ["foo","bar"];
	// this.viewValue   = "foo bar";
	// this.queryValue  = "foo&bar";

	var search          = {};
	search.searchTerms  = [];
	search.viewValue    = undefined;
	search.queryValue   = undefined;

	return {
		getSearch: function(){
			return search;
		},

		updateSearch: function(newTerms){
			if(newTerms !== undefined && newTerms !== null) updateSearchVals(newTerms);
			return search;
		}
	};

	function updateSearchVals(valueToUpdateWith){
		if(typeof valueToUpdateWith === "string"){
			if(valueToUpdateWith.indexOf('&') > 0)
				updateSearchFromQueryValue(valueToUpdateWith);
			else
				updateSearchFromViewValue(valueToUpdateWith);
		}

		if(valueToUpdateWith.constructor === Array)
			updateSearchFromArray(valueToUpdateWith);
	}

	function updateSearchFromArray(arraySearchTerms){
		search.searchTerms      = arraySearchTerms;
		search.viewValue      = arraySearchTerms.join(' ');
		search.queryValue = arraySearchTerms.join('&');
	}

	function updateSearchFromQueryValue(queryValue){
		search.searchTerms      = queryValue.split('&');
		search.viewValue      = search.searchTerms.join(' ');
		search.queryValue = queryValue;
	}

	function updateSearchFromViewValue(viewValue){
		search.searchTerms      = viewValue.split(' ');
		search.viewValue      = viewValue;
		search.queryValue = search.searchTerms.join('&');
	}
}]);
