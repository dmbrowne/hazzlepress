angular.module('hazzlepress.search', ['hazzlepress.search.services'])

.config(['$stateProvider', function($stateProvider){
	$stateProvider
		.state('search',{
			url: '/search/:searchquery',
			templateUrl: 'search/search.tpl.html',
			controller: 'SearchController',
			controllerAs: 'searchCtrlr',
			reloadOnSearch: false,
			resolve: {
				SearchQuery: 'SearchQuery',

				searchTerms: function($stateParams, SearchQuery){
					SearchQuery.updateSearch($stateParams.searchquery);
					return SearchQuery.getSearch().searchTerms;
				},

				searchValue: function(SearchQuery){
					return SearchQuery.getSearch().viewValue;
				},

				returnStateName: function($state){
					return getState;

					function getState(){
						if($state.current.name === "search")
							return "search.all";
						else
							return $state.current.name;
					}
				}
			},
			params: {
				searchquery: {
					value: null,
					squash: true
				}
			}
		})

		.state('search.all',{
			url: "/",
			templateUrl: 'search/all.tpl.html',
			controller: 'TextSearchController',
			controllerAs: 'textSrchCtrlr',
			resolve: {
				PostsFactory: 'PostsFactory',

				pageNumber: function(){ return 1; },

				initSearchPosts: function(searchTerms, pageNumber, PostsFactory){
					if(searchTerms === undefined || searchTerms.length < 1)
						return [];

					return PostsFactory.search(searchTerms,{
						per_page: 10,
						page_number: pageNumber
					}).$promise;
				}
			}
		})

		.state('search.tags',{
			url: "/tags",
			templateUrl: 'search/tags.tpl.html',
			controller: 'TagSearchController',
			controllerAs: 'tagSrchCtrlr',
			resolve: {
				PostsFactory: 'PostsFactory',

				pageNumber: function(){ return 1; },

				initTaggedPosts: function(searchTerms, pageNumber, PostsFactory){
					if(searchTerms === undefined || searchTerms.length < 1)
						return [];

					return PostsFactory.searchHashtags(searchTerms,{
						per_page: 10,
						page_number: pageNumber
					}).$promise;
				}
			}
		});

}])

.controller('SearchController', ['searchValue', '$state', 'SearchQuery', '$scope', 'returnStateName', function(searchValue, $state, SearchQuery, $scope, returnStateName){
	var self = this;
	this.searchValue  = searchValue;
	this.searchQuery  = SearchQuery.queryValue;
	this.searchState  = returnStateName();
	this.submitSearch = submitSearch;

	$scope.$watch(
		function(){
			return self.searchValue;
		},
		function(newValue, oldVal){
			if(newValue === oldVal) return;
			self.searchQuery = newValue.trim();
			self.searchQuery = self.searchQuery.replace(/\s+/g, "&");
		}
	);

	function submitSearch(form){
		if(form.$valid){
			$state.go(self.searchState, {searchquery: self.searchQuery});
		}
	}
}])

.controller('TextSearchController', ['PostsFactory', 'pageNumber', 'initSearchPosts', function (PostsFactory, pageNumber, initSearchPosts){
	this.posts = initSearchPosts;
}])

.controller('TagSearchController', ['PostsFactory', 'pageNumber', 'initTaggedPosts', function (PostsFactory, pageNumber, initTaggedPosts){
	this.posts = initTaggedPosts;
}])

;
