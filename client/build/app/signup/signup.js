'use strict';

angular.module('hazzlepress.signup', [])

.config(['$stateProvider', function($stateProvider){
	$stateProvider.state('signup',{
		url: "/signup",
		templateUrl: 'signup/signup.tpl.html',
		controller: 'SignUpController',
		controllerAs: 'signupCntrlr'
	});
}])

.controller('SignUpController', ['UserFactory', function(UserFactory){
	var self = this;

	this.passwordInputType = 'password';

	this.showHidePassword = function(){
		if (self.passwordInputType == 'password')
			self.passwordInputType = 'text';
		else
			self.passwordInputType = 'password';
	};

	this.addUser = function(form){
		if(form.$valid){
			UserFactory.addUser({
				email: form.email.$viewValue,
				password: form.password.$viewValue
			})
			.then(function(newUser){
				console.log(newUser);
			});
		}
	};
}])


;