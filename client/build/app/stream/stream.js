(function(){
	'use strict'

	angular
		.module('hazzlepress.stream', [])
		.config(streamconfiguration)


	streamconfiguration.$inject = ['$stateProvider']

	function streamconfiguration($stateProvider){
		$stateProvider.state('stream', {
			url: "/stream",
			templateUrl: 'stream/stream.tpl.html',
			controller: 'StreamController',
			controllerAs: 'streamCntrlr',
			resolve: {
				UserFactory: 'UserFactory',

				PostsFactory: 'PostsFactory',

				offsetNo: function(){ return 1 },

				currentUser: function(UserFactory) {
					return UserFactory.returnUser()
				},

				initPosts: function(PostsFactory, offsetNo){
					return PostsFactory.getAll({
						page_number: offsetNo,
						per_page: 10
					}).then(function(posts){
						return posts
					})
				}
			}
		})
	}
})()
