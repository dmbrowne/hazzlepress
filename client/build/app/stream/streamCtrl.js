(function(){
	'use strict'

	angular
		.module('hazzlepress.stream')
		.controller('StreamController', StreamController)



	StreamController.$inject = ['_', '$scope', 'PostsFactory', 'currentUser', 'initPosts', 'offsetNo']

	function StreamController(_, $scope, PostsFactory, currentUser, initPosts, offsetNo){
		var self = this;
		this.isLoggedIn     = false
		this.currentUser    = currentUser
		self.posts          = initPosts
		self.loadPostOffset = offsetNo
		self.loadMorePosts  = loadMorePosts
		self.postsLoading   = false

		$scope.$watchCollection(
			function(){
				return self.currentUser
			},
			function(newV){
				if(Object.keys(newV).length)
					self.isLoggedIn = true;
				else
					self.isLoggedIn = false;
			}
		)

		function loadMorePosts(){
			if(self.postsLoading) return

			self.postsLoading = true
			self.loadPostOffset++

			PostsFactory.getAll({
				page_number: self.loadPostOffset,
				per_page: 10
			}).then(function(posts){
				_.forEach(posts, function(post){
					self.posts.push(post)
				})
				self.postsLoading = false
			});
		}
	}

})()
