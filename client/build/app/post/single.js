angular.module('hazzlepress.singlepost', ['hazzlepress.singlepost.services', 'ui.bootstrap'])

	.config(['$stateProvider', function($stateProvider){
		$stateProvider
			.state('post',{
				url:          "/post/:post_id",
				abstract:     true,
				templateUrl:  'post/post.tpl.html',
				controller:   'SinglePostController',
				controllerAs: 'singlePostcntrlr',
				resolve: {
					PostsFactory: 'PostsFactory',

					postId: function($stateParams){
						return $stateParams.post_id;
					},

					post: function(PostsFactory, postId){
						return PostsFactory.getSinglePost(postId)
							.then(function(post){
								return post;
							});
					}
				}
			})

			.state('post.view',{
				url:          '',
				templateUrl:  'post/post-view.tpl.html',
				controller:   'ViewPostController',
				controllerAs: 'viewPostCntrlr',
				resolve: {

					CommentsFactory: 'CommentsFactory',

					commentPageOffset: function(){ return 1; },

					latestComments: function(CommentsFactory, postId, commentPageOffset){
						return CommentsFactory.getForPost(postId, {
							per_page: 15,
							page_number: commentPageOffset
						})
						.then(function(comments){
							return comments;
						});
					}
				}
			})

			.state('post.edit',{
				url: "/edit",
				templateUrl: 'post/post-edit.tpl.html',
				controller: 'EditPostController',
				controllerAs: 'editPostCntrlr',
				resolve: {

					_ : '_',

					userFactory: 'UserFactory',

					EmotionsFactory: 'EmotionsFactory',

					currentEmotions: function(post, _){
						return _.pluck(post.Emotions, 'id');
					},

					allemotions: function(EmotionsFactory){
						return EmotionsFactory.getEmotions().then(function(emos){
							return emos.emotions;
						});
					},

					editPreForm: function(post, _){
						var editForm = {};
						editForm.description = post.description;

						if(post.Media){
							editForm.deleteMedia = [];
							var postMediaIds = _.pluck(post.Media, 'id');
							_.forEach(postMediaIds, function(mediaid){
								var obj = {};
								obj[mediaid] = true;
							});
						}
						return editForm;
					},

					currentUser: function(userFactory){
						return userFactory.returnUser();
					}
				}
			})
		;
	}])

	.controller('SinglePostController', ['postId', 'post', function(postId, post){
		this.post = post;
		this.postId = postId;
	}])

	.controller('ViewPostController',
		['postId', 'PostsFactory', 'CommentsFactory', 'latestComments', 'commentPageOffset', '$modal',
		function(postId, PostsFactory, CommentsFactory, latestComments, commentPageOffset, $modal){
		var self = this;
		this.comments          = latestComments;
		this.commentPageOffset = commentPageOffset;
		this.loadMoreComments  = loadMoreComments;
		this.delete            = confirmDelete;

		function confirmDelete(){
			$modal.open({
				templateUrl: 'post/deletePostConfirm.tpl.html'
			})
			.result.then(
				function(){
					PostsFactory.delete(postId).then(
						function success(){
							alert('deleted');
						},
						function removeError(err){
							alert('error');
							console.log(err);
						}
					);
				},
				function(){
					alert('dismissed');
				}
			);
		}

		function loadMoreComments(){
			self.commentPageOffset++;

			CommentsFactory.getForPost(postId, {
				per_page: 15,
				page_number: self.commentPageOffset
			})
			.then(function(comments){
				self.comments.push(comments);
			});
		}
	}])

	.controller('EditPostController', ['postId', 'currentEmotions', 'allemotions', '_', 'editPreForm', 'currentUser', 'PostsFactory', function(postId, currentEmotions, allemotions, _, editPreForm, currentUser, PostsFactory){
		var self = this;
		this.isCurrentEmotion          = isCurrentEmotion;
		this.emotions                  = allemotions;
		this.editPostForm              = editPreForm;
		this.editPostForm.deleteMedia  = [];
		this.editPostForm.newMediaList = [];
		this.submitUpdateForm          = formSubmit;
		this.addObjectToNewMediaList   = addNewObjectToMediaList;
		this.user = currentUser;


		function addNewObjectToMediaList(){
			if( self.editPostForm.newMediaList.length <= 0 )
				return self.editPostForm.newMediaList.push({});

			if( _.size( _.last(self.editPostForm.newMediaList) ) > 0 )
				return self.editPostForm.newMediaList.push({});
		}

		function formSubmit(form){
			if(!form.$valid){ alert('not valid'); return; }

			var dataForSubmit = createObjectForSubmit(self.editPostForm);
			PostsFactory.update(postId, dataForSubmit, currentUser.token)
				.then(
					function(updatedPost){
						alert('updated');
					},
					function(err){
						alert(err.message);
						console.log(err);
					}
				);
		}

		function createObjectForSubmit(fullobj){
			var objForSubmit = {};
			objForSubmit.user_id = currentUser.user.id;
			objForSubmit.description = fullobj.description;
			objForSubmit.emotion_id  = _.pluck(fullobj.emotionIds, 'id');

			if(fullobj.deleteMedia.length)
				objForSubmit.deleteMedia  = _.pluck(fullobj.deleteMedia, 'id');

			if(fullobj.newMediaList.length && _.size(fullobj.newMediaList[0]) )
				objForSubmit.media  = _.pluck(fullobj.newMediaList, 'base64String');

			return objForSubmit;
		}

		function isCurrentEmotion(emotionIdToCheck){
			return (_.indexOf(currentEmotions, emotionIdToCheck) >= 0 ? true : false);
		}
	}])

	;
