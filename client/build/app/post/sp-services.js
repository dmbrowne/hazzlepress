angular.module('hazzlepress.singlepost.services', [])

	.factory('CommentsFactory', ['$q', '_', 'CommentsApi', function($q, _, CommentsApi){
		var factory = {};

		factory.getAll = function(options){
			var deferred            = $q.defer();

			return deferred.promise;
		};

		factory.getForPost = function(postid, options){
			var deferred        = $q.defer();
			var retrivalOptions = {};

			if(options){
				if(options.page_number)
					retrivalOptions.pagenumber = options.page_number;

				if(options.offset)
					retrivalOptions.offset = options.offset;

				if(!_.isEmpty(retrivalOptions))
					postRetrival = JSON.stringify(retrivalOptions);
			}

			CommentsApi.post()
				.get({post_id: postid}, retrivalOptions)
				.$promise.then(deferred.resolve);

			return deferred.promise;
		};

		factory.getForUser = function(userid, options){
			var deferred        = $q.defer();
			var retrivalOptions = {};

			if(options){
				if(options.page_number)
					retrivalOptions.pagenumber = options.page_number;

				if(options.offset)
					retrivalOptions.offset = options.offset;

				if(!_.isEmpty(retrivalOptions))
					postRetrival = JSON.stringify(retrivalOptions);
			}

			CommentsApi.user()
				.get({user_id: userid}, retrivalOptions)
				.$promise.then(deferred.resolve);

			return deferred.promise;
		};

		return factory;
	}]);