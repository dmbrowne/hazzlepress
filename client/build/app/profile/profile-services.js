angular.module('hazzlepress.profile.services', [])

	.service('ProfileService', ['_', function(_){
		this.convertFlatObjectToProfileUserObject = convertFlatObjectToProfileUserObject;

		function convertFlatObjectToProfileUserObject(flatObj){
			var updateObj = {};
			var profileUpdateObj = {};

			if(flatObj.email)
				updateObj.email = flatObj.email;

			if(flatObj.password)
				updateObj.password = flatObj.password;

			if(flatObj.username)
				profileUpdateObj.username = flatObj.username;

			if(flatObj.username)
				profileUpdateObj.username = flatObj.username;

			if(flatObj.name)
				profileUpdateObj.name = flatObj.name;

			if(flatObj.bio)
				profileUpdateObj.bio = flatObj.bio;

			if(flatObj.website)
				profileUpdateObj.website = flatObj.website;

			if(flatObj.phone)
				profileUpdateObj.phone = flatObj.phone;

			if(flatObj.gender)
				profileUpdateObj.gender = flatObj.gender;

			if(flatObj.private)
				profileUpdateObj.private = flatObj.private;

			if(_.size(profileUpdateObj) > 0)
				updateObj.profile = profileUpdateObj;

			return updateObj;
		}

	}])
;
