"use strict";

angular.module('hazzlepress.profile', ['hazzlepress.profile.services', 'hazzlepress.profile.userposts'])

.config(['$stateProvider', function($stateProvider){
	$stateProvider

		.state('profile', {
			abstract: true,
			url: "/profile/:user_id",
			templateUrl: 'profile/profile.tpl.html',
			controller: 'UserProfileController',
			controllerAs: 'profileCntrlr',
			resolve: {

				UserFactory: 'UserFactory',

				userId: function($stateParams){
					return $stateParams.user_id;
				},

				Profile: function(UserFactory, userId){
					return UserFactory.getProfileByUserId(userId);
				}
			}
		})

		.state('profile.view', {
			url: "",
			templateUrl: 'profile/view-profile.tpl.html',
			controller: 'ViewUserProfileController',
			controllerAs: 'profileViewCntrlr'
		})

		.state('profile.edit', {
			url: "/edit",
			templateUrl: 'profile/edit-profile.tpl.html',
			controller: 'EditUserProfileController',
			controllerAs: 'editprofileCtrlr',
			resolve: {

				InitialValues: function(Profile, _){
					var initalValues = {};

					initalValues.email = Profile.email;

					_.forEach(Profile.Profile, function(val, key){
						initalValues[key] = val;
					});

					return initalValues;
				}
			}
		});

}])

.controller('UserProfileController', ['Profile', function(Profile){
	this.profile = Profile;
}])

.controller('ViewUserProfileController', [function(){
	//
}])

.controller('EditUserProfileController', ['$scope', 'InitialValues', '_', 'ProfileService', 'UserFactory', 'userId', '$window',
	function($scope, InitialValues, _, ProfileService, UserFactory, userId, $window){
		var self = this;

		// flattened version from $resource and contain a promise
		this.initalValues = InitialValues;

		// ngModel values on edit form
		this.editForm = {};

		this.newProfilePhoto = undefined;

		this.submitProfileUpdate = submitProfileUpdate;

		$scope.$watch(function(){
			return self.newProfilePhoto;
		}, function(newV, oldV){
			if(newV !== undefined && newV !== oldV) submitUserPhoto();
		});

		function submitUserPhoto(){
			if(self.newProfilePhoto === undefined){
				$window.alert('the profile photo variable is undefined');
				return;
			}

			UserFactory.updateProfile(userId, {
					profile:{
						profile_photo: [self.newProfilePhoto]
					}
				})
				.then(function(updatedProfile){
					self.newProfilePhoto = undefined;
				});
		}

		function submitProfileUpdate(){
			var newVals = {};

			_.forEach(self.editForm, function(val, key){
				var initalVal = self.initalValues[key];
				if(initalVal === undefined) newVals[key] = val;
				if(initalVal !== val) newVals[key] = val;
			});

			if(_.size(newVals) <= 0) return;

			var profileUpdateObject = ProfileService.convertFlatObjectToProfileUserObject(newVals);

			UserFactory.updateProfile(userId, profileUpdateObject)
				.then(function(updatedProfile){
					$window.alert('updated');
				}, function(){
					$window.alert('error updated');
				});
		}

	}
])

;
