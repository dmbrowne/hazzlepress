'use strict';

angular.module('hazzlepress.profile.userposts', ['ui.bootstrap'])

.config(['$stateProvider', function($stateProvider){

	$stateProvider.state('profile.posts', {
		url: "/posts",
		templateUrl: 'profile/posts/userposts.tpl.html',
		controller: 'UserPostsController',
		controllerAs: 'userPostsCtrlr',
		resolve: {

			PostsFactory: 'PostsFactory',

			postsPageNumber: function(){
				return 1;
			},

			userPosts: function(PostsFactory, userId, postsPageNumber){
				return PostsFactory.byUser(userId, {
					per_page: 15,
					page_number: postsPageNumber
				}).$promise;
			}
		}
	});

}])

.controller('UserPostsController',

	['PostsFactory', 'userPosts', 'postsPageNumber', 'userId', '_', '$modal', '$q',

	function(PostsFactory, userPosts, postsPageNumber, userId, _, $modal, $q){
		var self = this;
		this.posts             = userPosts;
		this.currentPageNumber = postsPageNumber;
		this.loadMore          = loadMorePosts;
		this.selectedPosts     = [];
		this.multiDelete       = confirmDelete;
		this.confirmSingleDelete = function(postid){
			deletePosts([postid]).then(deletePostSuccess, deletePostError);
		};

		function confirmDelete(){
			$modal.open({
				templateUrl: 'profile/posts/deletePostConfirm.tpl.html'
			})
			.result
				.then(deletePosts)
				.then(deletePostSuccess, deletePostError);
		}

		function loadMorePosts(){
			self.currentPageNumber++;

			return PostsFactory.byUser(userId, {
				per_page: 15,
				page_number: self.currentPageNumber
			})
			.then(function(posts){
				self.posts.push(posts);
			}, function(err){
				alert(err);
				console.error(err);
			});
		}

		function deletePosts(postids){
			var deferred  = $q.defer();
			var deletepost;

			postids = postids || _.pluck(self.selectedPosts, 'id');
			if(postids.length <= 0) deferred.reject('postids array is empty');

			if(postids.length === 1)
				deletepost = PostsFactory.delete(postids[0]);
			else
				deletepost = PostsFactory.deleteMultiple(postids);

			deletepost.then(function(){
				deferred.resolve(postids);
			}, function(err){
				deferred.reject(err);
			});

			return deferred.promise;
		}

		function deletePostSuccess(deletedPostids){
			_.forEach(deletedPostids, function(postid){
				var deletedPostIndex = _.findIndex(self.posts, {'id': postid});
				self.posts.splice(deletedPostIndex, 1);
			});
		}

		function deletePostError(err){
			alert('error on delete');
			console.log(err);
		}
	}]
)
;
