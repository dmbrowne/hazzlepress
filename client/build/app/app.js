// window.addEventListener('load', function () {
//   	FastClick.attach(document.body);
// }, false);


angular.module('hazzlepress', [
	'angularMoment',
	'app-templates',
	'common-templates',
	'infinite-scroll',
	'ui.router',
	'ngAnimate',
	'ngResource',
	'ngSanitize',
	'hazzlepress.api',
	'hazzlepress.common.services',
	'hazzlepress.common.directives',
	'hazzlepress.common.filters',
	'hazzlepress.stream',
	'hazzlepress.createpost',
	'hazzlepress.singlepost',
	'hazzlepress.signup',
	'hazzlepress.login',
	'hazzlepress.profile',
	'hazzlepress.search'
])

.config(['$urlRouterProvider', '$locationProvider', function($urlRouterProvider, $locationProvider){
	$urlRouterProvider.otherwise("/stream");
	$locationProvider.html5Mode(true);
}])

.run(['$rootScope', '$state', 'amMoment', function($rootScope, $state, amMoment){
	$rootScope.$state = $state;
	amMoment.changeLocale('en');
}])
;
