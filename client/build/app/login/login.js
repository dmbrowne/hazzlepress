'use strcit';

angular.module('hazzlepress.login', [])

.config(['$stateProvider', function($stateProvider){
	$stateProvider.state('login',{
		url: "/login",
		templateUrl: 'login/login.tpl.html',
		controller: 'LoginController',
		controllerAs: 'loginCntrlr'
	});
}])

.controller('LoginController', ['UserFactory', '$scope', '$state', function(UserFactory, $scope, $state){
	var self = this;

	this.passwordInputType = 'password';
	this.usernameOrEmailInputType = 'text';

	$scope.$watch('loginCntrlr.emailorusername', function(newV, oldV){
		if(newV === undefined || newV === oldV)
			return;

		if(newV.indexOf("@") > 0){
			if(self.usernameOrEmailInputType !== 'email')
				self.usernameOrEmailInputType = 'email';
		}else{
			if(self.usernameOrEmailInputType !== 'text')
				self.usernameOrEmailInputType = 'text';
		}
	});

	this.showHidePassword = function(){
		if (self.passwordInputType == 'password')
			self.passwordInputType = 'text';
		else
			self.passwordInputType = 'password';
	};

	this.login = function(form){
		if(form.$valid){
			var loginObj = {};
			var loginTypeKey;

			if(self.usernameOrEmailInputType === 'email')
				loginTypeKey = 'email';
			else
				loginTypeKey = 'username';

			loginObj[loginTypeKey] = form.emailorusername.$viewValue;
			loginObj.password      = form.password.$viewValue;
			loginObj.remember      = form.rememberMe.$viewValue;

			UserFactory.login(loginObj)
				.then(
					function(res){
						$state.go('stream');
					},
					function(res){
						alert(res.data.message);
					}
				);
		}
	};

}]);