angular.module('hazzlepress.createpost', ['angularFileUpload'])
	.config(['$stateProvider', function($stateProvider){
		$stateProvider.state('stream.createpost',{
			url: "/create",
			templateUrl: 'create-post/create-post.tpl.html',
			controller: 'CreatePostController',
			controllerAs: 'createCtrlr',
			resolve: {
				EmotionsFactory: 'EmotionsFactory',

				UserFactory: 'UserFactory',

				currentUser: function (UserFactory) {
					return UserFactory.returnUser()
				},

				emotions: function (EmotionsFactory) {
					return EmotionsFactory.getEmotions().then(function(emoObj){
						return angular.copy(emoObj.emotions)
					})
				}
			}
		});
	}])

	.controller('CreatePostController', [
		'_', 'PostsFactory', 'emotions', 'currentUser', '$state',
		function(_, PostsFactory, emotions, currentUser, $state){
			var self = this;

			self.currentUser = currentUser
			self.emotions    = emotions
			this.emotionIds  = {}
			this.mediaList   = []
			this.submitPost  = submitPost


			function submitPost(form){
				if(!form.$valid)
					return alert('form is invalid');

				if(_.size(self.emotionIds) < 1)
					return alert('please choose some emotions');


				var emotionsids     = [];
				var postDescription = form.description.$viewValue;
				var userId          = self.currentUser.user.id;

				_.forEach(self.emotionIds, function(booleanV, emotionID){
					if(booleanV === true) emotionsids.push(emotionID);
				});

				postInsertObject = {
					emotion_id: emotionsids,
					user_id: userId,
					description: postDescription
				};

				if(self.mediaList.length)
					postInsertObject.media = _.pluck(self.mediaList, 'base64String')


				PostsFactory.create(postInsertObject, self.currentUser.token).then(
					function successfullyCreatedPost(){
						alert('created!');
						$state.go('stream');
					},
					function errorCreatingPost(error){
						console.log(error);
						alert('error');
					},
					function creatingPostProgress(){

					}
				);
			}
		}
	])

	;
