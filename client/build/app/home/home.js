angular.module('hazzlepress.home', [])
	.config(['$stateProvider', function($stateProvider){
		$stateProvider.state('home',{
			url: "/landing",
			templateUrl: 'home/home.tpl.html',
			controller: 'HomeController'
		});
	}])

	.controller('HomeController', ['$scope', function($scope){
		//
	}])
	;