'use strict';

angular.module('hazzlepress.common.filters', ['ngSanitize'])

.filter('tweetLinky',['$filter', '$sce',
	function($filter, $sce) {
		return function(text, target) {
			if (!text) return text;

			var replacedText = $filter('linky')(text, target);
			var targetAttr = "";
			if (angular.isDefined(target)) {
				targetAttr = ' target="' + target + '"';
			}
			// replace #hashtags and send them to twitter
			var replacePattern1 = /(^|\s)#(\w*[a-zA-Z_]+\w*)/gim;
			// replacedText = text.replace(replacePattern1, '$1<a ui-sref="search.all({searchquery: \'%23$2\'})" ' + targetAttr + '>#$2</a>');
			replacedText = text.replace(replacePattern1, '$1<a href="search/$2/tags" ' + targetAttr + '>#$2</a>');
			// replace @mentions but keep them to our site
			var replacePattern2 = /(^|\s)\@(\w*[a-zA-Z_]+\w*)/gim;
			replacedText = replacedText.replace(replacePattern2, '$1<a href="https://twitter.com/$2"' + targetAttr + '>@$2</a>');

			return $sce.trustAsHtml(replacedText);
		};
	}
]);
