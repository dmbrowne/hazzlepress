'use strict';

angular
	.module('hazzlepress.api')
	.factory('PostsApi', PostsApi)

PostsApi.$inject = ['$resource']

function PostsApi($resource){
	var PostsAPI = {
		getAll:       getAll,
		getByUser:    getByUser,
		create:       create,
		single:       single,
		multiDelete:  multiDelete,
		searchall:    searchall,
		searchtags:   searchtags
	};

	return PostsAPI;

	function getAll(){
		return $resource('http://localhost:3000/api/v1/posts',{},{
			get: { method: 'POST', isArray: true }
		})
	}

	function getByUser(){
		return $resource('http://localhost:3000/api/v1/posts/user/:user_id', {}, {
			'obtain': { method: 'POST', isArray: true }
		})
	}

	function create(bearerID){
		return $resource('http://localhost:3000/api/v1/posts/create', {}, {
			'save':{
				method: 'POST',
				headers: {'Authorization':'Bearer '+ bearerID}
			}
		})
	}

	function single(bearerID){
		return $resource('http://localhost:3000/api/v1/posts/:post_id',{},{
			'update': {
				method: 'PUT',
				isArray: false,
				headers: {'Authorization':'Bearer '+ bearerID}
			},
			'fetch': {
				method: 'GET',
				isArray: true
			},
			'remove': {
				method: 'DELETE',
				isArray: false,
				headers: {'Authorization':'Bearer '+ bearerID}
			}
		})
	}

	function multiDelete(bearerID){
		return $resource('http://localhost:3000/api/v1/posts/remove',{},{
			'delete': {
				method: 'PUT',
				isArray: false,
				headers: {'Authorization':'Bearer ' + bearerID}
			}
		})
	}

	function searchall (requestBody) {
		return $resource('http://localhost:3000/api/v1/posts/search',{},{
			'obtain': { method: 'POST', isArray: true }
		})
	}

	function searchtags (requestBody) {
		return $resource('http://localhost:3000/api/v1/posts/tags',{},{
			'obtain': { method: 'POST', isArray: true }
		})
	}
}
