
(function(){
	'use strict';

	angular
		.module('hazzlepress.api')
		.factory('EmotionsApi', EmotionsApi)


	EmotionsApi.$inject = ['$resource']

	function EmotionsApi($resource){
		return {
			getAll: function(){
				return $resource('http://localhost:3000/api/v1/emotions');
			},

			postFeedback: function(bearerId){
				return $resource('http://localhost:3000/api/v1/postfeedback/:post_id', {},{
					'query': {
						headers: { 'Authorization' : 'Bearer ' + bearerId },
						isArray: true
					},
					'addremove': {
						method: 'POST',
						headers: { 'Authorization' : 'Bearer ' + bearerId },
						isArray: true
					}
				})
			}
		}
	}
})();
