'use strict';

angular
	.module('hazzlepress.api')
	.factory('UserApi', UserApi)

UserApi.$inject = ['$resource']

function UserApi($resource){
	var userAPI = {
		getToken: getToken,
		sessionUser: sessionUser,
		create: create,
		userWithProfile: userWithProfile,
	};

	return userAPI;

	function getToken(){
		return $resource('http://localhost:3000/api/v1/login',{},{
			'obtain': { method: 'POST' }
		})
	}

	function sessionUser(bearerID){
		return $resource('http://localhost:3000/api/v1/users/current',{},{
			get_token_user:{
				method: 'GET',
				headers: {'Authorization':'Bearer '+ bearerID}
			}
		})
	}

	function create(){
		return $resource('http://localhost:3000/api/v1/users');
	}

	function userWithProfile(){
		return $resource('http://localhost:3000/api/v1/users/:user_id', null,
			{
				'update': { method: 'PUT' }
			}
		)
	}
}
