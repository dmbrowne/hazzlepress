'use strict'

angular
	.module('hazzlepress.common.directives')
	.directive('hzpAddMedia', hzpaddmediaDirective)
	.directive('hzpAddMediaSelect', hzpAddMediaSelect)

hzpaddmediaDirective.$inject = ['_']

function hzpaddmediaDirective(_){
	return {
		restrict: 'E',
		bindToController: true,
		controllerAs: 'addmediaCntrlr',
		scope: {
			mediaList: '=',
			addMediaLabel: '@'
		},
		template:
			'<div ng-repeat="newMedia in addmediaCntrlr.mediaList">' +
				'<hzp-add-media-select media-item="newMedia"></hzp-add-media-select> ' +
			'</div>' +
			'<button class="btn btn-default" ng-click="addmediaCntrlr.addObjectToNewMediaList()">' +
				'{{addmediaCntrlr.addMediaLabel}}' +
			'</button>',
		controller: function(){
			var self       = this;
			self.mediaList = [];
			self.addObjectToNewMediaList = function(){
				var mediaListLength = self.mediaList.length
				if( mediaListLength < 1 ||
					_.size( self.mediaList[ (mediaListLength - 1) ] ) > 0 )
						self.mediaList.push({})
			}
		}
	}
}

function hzpAddMediaSelect(){
	return {
		retrict: 'E',
		scope: {
			mediaItem: '='
		},
		controllerAs: 'addMediaSelectCtrl',
		bindToController: true,
		templateUrl: 'common/templates/add-media.tpl.html',
		controller: function(){
			var self = this;

			this.setMediaItem = function(datastring){
				self.mediaItem.base64String = datastring;
			};
		}
	};
}