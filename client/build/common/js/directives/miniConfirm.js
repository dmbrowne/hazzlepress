'use strict'

angular.module('hazzlepress.common.directives')

.directive('hzpMiniConfirm', function(){
	return {
		restrict: 'EA',
		scope:{
			primaryAction: '&',
			secondaryAction: '&'
		},
		controllerAs: 'miniconfirmCntrlr',
		bindToController: true,
		controller: function($scope, $element, $attrs){
			var self = this;

			this.toggleActivation = function(){
				if(self.choiceActive === true)
					self.choiceActive = false;
				else
					self.choiceActive = true;
			};

			this.activateChoices = function(){
				$scope.$apply(function(){
					self.choiceActive = true;
				});
			};

			this.deactivateChoices = function(){
				self.choiceActive = false;
			};
		},
		link: function(scope, element, attrs){
		}
	};
})