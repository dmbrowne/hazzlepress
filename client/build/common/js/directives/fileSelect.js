angular.module('hazzlepress.common.directives')

.directive("ngFileSelect", ['fileReaderAPI', function(fileReaderAPI){
	return {
		require: 'ngModel',
		retrict: 'A',
		link: function($scope, element, attrs, ngModelController){
			$scope.renderFile = function (fileToRead) {
				fileReaderAPI.readData(fileToRead).then(function(result) {
					ngModelController.$setViewValue(result);
				});
			};

			element.bind("change", function(e){
				$scope.file = (e.srcElement || e.target).files[0];
				$scope.renderFile($scope.file);
			});
		}
	};
}])