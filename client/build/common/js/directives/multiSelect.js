'use strict'

angular.module('hazzlepress.common.directives')
	.directive('hzpMultiSelect', multiSelectDirective)
	.directive('hzpItemSelect', hzpItemSelect)
	
multiSelectDirective.$inject = ['$timeout', '_']

function multiSelectDirective($timeout, _){
	return {
		restrict: 'A',
		scope:{
			selectedItems: '='
		},
		controller: function($scope, $element, $attrs){
			var self = this;
			this.selected = [];
			$scope.selectedItems = this.selected;

			this.toggleSelect = function(dataObj){

				if(dataObj.selected && dataObj.selected === true)
					dataObj.selected = false;
				else
					dataObj.selected = true;

				addRemoveFromSelection(dataObj);
			};

			this.addToSelection = function(dataObj){
				var obj = angular.copy(dataObj);
				dataObj.selected = true;

				$timeout(function(){
					self.selected.push(obj);
				});
			};

			function addRemoveFromSelection(dataobj){
				var obj = angular.copy(dataobj);
				delete obj.selected;

				var positionInSelectionArray = _.findIndex(self.selected, obj);

				if(positionInSelectionArray >= 0){
					$timeout(function(){
						self.selected.splice(positionInSelectionArray, 1);
					});
				}else{
					$timeout(function(){
						self.selected.push(obj);
					});
				}
			}
		},
		link: function(scope, element, attrs, multiSelectCntrlr){
			scope.selected = multiSelectCntrlr.selected;
		}
	};
}

function hzpItemSelect(){
	return {
		restrict: 'A',
		require:  '^hzpMultiSelect',
		scope: {
			selectContext: '=',
			selectInit: '&'
		},
		link: function(scope, element, attrs, multiSelectCntrlr){
			if(scope.selectInit !== undefined){
				if(scope.selectInit() === true)
					multiSelectCntrlr.addToSelection(scope.selectContext);
			}

			angular.element(element).on('click', function(){
				multiSelectCntrlr.toggleSelect(scope.selectContext);
			});
		}
	};
}