'use strict'

angular
	.module('hazzlepress.common.directives')
		.directive('hzpPostFeedback', hzpPostFeedback)
		.directive('hzpPostFeedbackButton', hzpPostFeedbackButton)

hzpPostFeedback.$inject = ['EmotionsFactory', 'UserFactory', '_', 'EmotionsApi', '$q', '$timeout']
hzpPostFeedbackButton.$inject = ['_', '$window', '$timeout']

function hzpPostFeedback(EmotionsFactory, UserFactory, _, EmotionsApi, $q, $timeout){
	return {
		restrict: 'E',
		scope:{
			postId: '='
		},
		templateUrl:      'common/templates/postfeedback.tpl.html',
		controller:       hzpPostFeedbackCtrl,
		controllerAs:     'postfeedbackCtrlr',
		bindToController: true
	}

	function hzpPostFeedbackCtrl(){
		var self = this

		self.setFeedback               = setFeedback
		self.setEmotionIdsToSend       = setEmotionIdsToSend
		self.updateUserEmotionFeedback = updateUserEmotionFeedback

		self.currentUser = UserFactory.returnUser()
		self.emotions = []

		var emotionPromise = []
		emotionPromise.push( EmotionsFactory.getEmotions() )

		if(_.size(self.currentUser) > 0)
			emotionPromise.push(EmotionsApi.postFeedback(self.currentUser.token).query({post_id: self.postId}).$promise)

		$q.all(emotionPromise).then(function(promiseReturn){
			var emotionsModel     = angular.copy(promiseReturn[0].emotions),
				userPostFeedbacks = promiseReturn[1]

			self.emotions = _.filter(emotionsModel, {is_voteable: true})

			if(_.size(self.currentUser) && userPostFeedbacks.length)
				self.updateUserEmotionFeedback(userPostFeedbacks)
		})


		function updateUserEmotionFeedback(userFeedback){
			var userEmotions = _.pluck(userFeedback, 'id')

			_.forEach(self.emotions, function(emotionModelValue){
				if ( _.indexOf(userEmotions, emotionModelValue.id) >= 0 )
					emotionModelValue.active = true
				else
					emotionModelValue.active = false
			})
		}

		function setFeedback(emotionid){
			var deferred = $q.defer()

			if( _.size(self.currentUser) < 1 ) deferred.reject('user not logged in')

			if( self.sendFeedbackTimer ) $timeout.cancel(self.sendFeedbackTimer)

			autoToggleAgreeDisagree(emotionid)
			self.setEmotionIdsToSend(emotionid)

			if(self.emotionsToSend.length < 1) deferred.reject('no emotion ids to add or remove')

			if(self.emotionsToSend.length > 0 && _.size(self.currentUser) > 0){
				self.sendFeedbackTimer = $timeout(function(){
					return EmotionsApi.postFeedback(self.currentUser.token).addremove({ post_id: self.postId }, {
						emotion_ids: self.emotionsToSend
					}).$promise
				}, 2500)

				self.sendFeedbackTimer.then(
					function(userFeedback){
						// self.updateUserEmotionFeedback(userFeedback)
						self.emotionsToSend = []
						deferred.resolve()
					},
					function(err){
						deferred.reject(err)
					}
				)
			}

			return deferred.promise
		}

		function setEmotionIdsToSend(emotionid){
			if(self.emotionsToSend === undefined){
				self.emotionsToSend = []
				return self.emotionsToSend.push(emotionid)
			}

			var emotionAlreadyExistsPosition = _.indexOf(self.emotionsToSend, emotionid)

			if( emotionAlreadyExistsPosition >= 0 )
				return self.emotionsToSend.splice(emotionAlreadyExistsPosition, 1)
			else
				return self.emotionsToSend.push(emotionid)
		}

		function autoToggleAgreeDisagree(emotionid){
			self.agreePos        = self.agreePos    || _.findIndex(self.emotions, {slug: 'agree'})
			self.disagreePos     = self.disagreePos || _.findIndex(self.emotions, {slug: 'disagree'})
			self.agreeEmotion    = self.agreeEmotion    || self.emotions[self.agreePos]
			self.disagreeEmotion = self.disagreeEmotion || self.emotions[self.disagreePos]

			// trying to set agree to true, so check if disagree is currently true - and if it is then set disagree to false
			if( emotionid === self.agreeEmotion.id && self.disagreeEmotion.active === true ){
				setEmotionIdsToSend(self.disagreeEmotion.id)
				self.disagreeEmotion.active = false
			}

			// trying to set disagree to true, so check if agree is currently true - and if it is then set agree to false
			if( emotionid === self.disagreeEmotion.id && self.agreeEmotion.active === true ){
				setEmotionIdsToSend(self.agreeEmotion.id)
				self.agreeEmotion.active = false
			}
		}

	}

}

function hzpPostFeedbackButton(_, $window, $timeout){
	return {
		require: '^hzpPostFeedback',
		restrict: 'A',
		scope: true,
		link: function(scope, el, attrs, hzpPostFbckCtrlr){
			el.on('click', function(){
				if( _.size(hzpPostFbckCtrlr.currentUser) <= 0 )
					return $window.alert('please log in to sentiment this')

				toggleActiveState()

				$timeout(function(){
					hzpPostFbckCtrlr
						.setFeedback(scope.emotion.id)
						.catch(function(err){
							if(err !== "canceled") toggleActiveState()
						})
				})
			})

			function toggleActiveState(){
				if(scope.emotion.active === true)
					scope.emotion.active = false
				else
					scope.emotion.active = true
			}
		}
	}
}
