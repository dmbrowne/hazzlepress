'use strict'

angular.module('hazzlepress.common.directives')

	.directive('hzpMiniConfirmActivate', hzpMiniConfirmActivate)
	.directive('hzpMiniConfirmChoices', hzpMiniConfirmChoices)
	.directive('hzpChoicePrimary', hzpChoicePrimary)
	.directive('hzpChoiceSecondary', hzpChoiceSecondary)

function hzpMiniConfirmActivate(){
	return {
		restrict: 'A',
		require: '^hzpMiniConfirm',
		link: function (scope, element, attrs, miniConfirmCtrl) {
			element.on('click', function(){
				miniConfirmCtrl.activateChoices();
			});
		}
	};
}

function hzpMiniConfirmChoices(){
	return {
		restrict: 'A',
		require: '^hzpMiniConfirm',
		link: function (scope, element, attrs, confirmCntrlr) {
			scope.$watch(
				function(){
					return confirmCntrlr.choiceActive;
				},
				function(val){
					if(val === true)
						angular.element(element).show();
					else
						angular.element(element).hide();
				}
			);
		}
	};
}

function hzpChoicePrimary(){
	return {
		restrict: 'A',
		require: '^hzpMiniConfirm',
		link: function(scope, element, attrs, cntrls){
			element.on('click', function(){
				cntrls.primaryAction();
			});
		}
	};
}

function hzpChoiceSecondary(){
	return {
		restrict: 'A',
		require: '^hzpMiniConfirm',
		link: function(scope, element, attrs, cntrls){
			element.on('click', function(){
				cntrls.secondaryAction();
			});
		}
	};
}