'use strict'

angular
	.module('hazzlepress.common.directives')
		.directive('hzpJcropWrap', hzpJcropWrap)
		.directive("hzpJcrop", hzpJcrop)

hzpJcrop.$inject = ['$timeout']

function hzpJcropWrap(){
	return {
		retrict: 'EA',
		controller: function($scope, $element, $attrs){
			this.width = $element[0].clientWidth;
			this.height = $element[0].clientHeight;
		},
		link: function(scope, el, attrs, cropWrapController){
			cropWrapController.width = el[0].clientWidth;
			cropWrapController.height = el[0].clientHeight;
		}
	}
}

function hzpJcrop($timeout){
	return {
		require: '^hzpJcropWrap',
		retrict: 'EA',
		scope:{
			imgSrc: '@src',
			coords: '='
		},
		link: function(scope, element, attrs, wrapperController){
			var maxWidth  = wrapperController.width;
			var maxHeight = wrapperController.height;

			attrs.$observe('src', function(val){
				if(typeof val !== 'string') return;

				angular.element(element).Jcrop({
					// boxWidth: maxWidth,
					// bgColor:     'black',
					// bgOpacity:   .4,
					boxWidth:    maxWidth,
					onSelect:    processCoords,
					setSelect:   [ 0, 0, (maxWidth/2), (maxWidth/2) ],
					aspectRatio: 1
					// aspectRatio: 4 / 3
				});
			});

			function processCoords(cropCoords){
				$timeout(function(){
					scope.coords = cropCoords;
				});
			}
		}
	};
}
