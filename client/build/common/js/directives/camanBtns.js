'use strict'

angular.module('hazzlepress.common.directives')


.directive('hzpCamanFilter', function(){
	return {
		restrict:'A',
		require: '^hzpCaman',
		link: function(scope, element, attrs, hzpCamanController){
			attrs.$observe('preset', function(filtername){
				element.on('click', function(){
					hzpCamanController.addFilter(filtername);
				});
			});
		}
	};
})

.directive('hzpCamanSave', function(){
	return {
		restrict:'A',
		require: '^hzpCaman',
		link: function(scope, element, attrs, hzpCamanController){
			element.on('click', function(){
				hzpCamanController.saveImageAsBase64();
			});
		}
	};
})