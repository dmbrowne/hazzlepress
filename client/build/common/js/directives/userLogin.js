
(function(){
	'use strict'

	angular
		.module('hazzlepress.common.directives')
		.directive('userHandler', userHandler)


	userHandler.$inject = ['UserFactory', '_', '$state']

	function userHandler(UserFactory, _, $state){
		return {
			restrict: 'EA',
			scope: true,
			templateUrl: 'common/templates/userHandle.tpl.html',
			controllerAs: 'userHandleCtrl',
			controller: userHandlerCtrlr
		}

		function userHandlerCtrlr($scope){
			var self = this;
			self.isLoggedIn  = false
			self.currentUser = UserFactory.returnUser()
			self.logOut      = self.logout
			self.user        = self.currentUser.user

			$scope.$watchCollection(
				function(){
					return self.currentUser
				},
				function(userObj){
					if(_.size(userObj) > 0)
						self.isLoggedIn = true
					else
						self.isLoggedIn = false
				}
			)
		}

		userHandlerCtrlr.protoype.logout = function(){
			UserFactory.logout()
			$state.go('stream')
		}
	}
})()
