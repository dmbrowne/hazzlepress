'use strict'

angular.module('hazzlepress.common.directives')

.directive('video', function videoDirective(){
	return {
		restrict: 'E',
		link: function(scope, element, attrs){
			var	videoElement  = element[0];
			scope.playing = false;
			scope.filename = undefined;

			attrs.$observe('source', function(val){
				scope.filename = val;
			});

			if(attrs.lazy !== undefined){
				scope.$watch('isVisible', function(isvisible, oldVal){
					if(isvisible === true && videoElement.currentSrc === '' || isvisible === true && videoElement.currentSrc === undefined){
						loadVid(scope.filename);
					}
				});
			}else{
				scope.$watch('filename', function(val){
					if(val === undefined) return;
					loadVid(val);
				});
			}

			element.on('click', function(){
				if(scope.filename !== undefined){
					if(scope.playing){
						pause();
					}else{
						if(videoElement.currentSrc === '' || videoElement.currentSrc === undefined)
							play(scope.filename);
						else
							resume();
					}
				}
			});

			function loadVid(file){
				videoElement.src = file;
			}

			function play(file){
				loadVid(file);
				videoElement.play();
				scope.playing = true;
			}

			function resume(){
				videoElement.play();
				scope.playing = true;
			}

			function pause(){
				videoElement.pause();
				scope.playing = false;
			}

			function stop(){
				videoElement.pause();
				videoElement.src = videoElement.currentSrc;
				scope.playing = false;
			}
		}
	};
})
