angular.module('hazzlepress.common.directives')

	.directive('hzpCamanCanvas',function(){
		return {
			restrict: 'EA',
			require: '^hzpCaman',
			scope:{
				coords: '=',
				imgSrc: '='
			},
			link: function(scope, element, attrs, hzpCamanController){
				scope.$watch('imgSrc', function(newV){
					if(typeof newV !== 'string') return;
					Caman(element[0], newV, function(){
						// width, height, x, y
						this.crop(
							// (scope.coords.x2 - scope.coords.x),
							// (scope.coords.y2 - scope.coords.y),
							scope.coords.w,
							scope.coords.h,
							scope.coords.x,
							scope.coords.y
						);
	
						// this.resize({
						// 	width: element[0].parentNode.clientWidth
						// });
	
						this.render();
	
						hzpCamanController.setCanvasEl(this);
					});
				});
			}
		};
	})