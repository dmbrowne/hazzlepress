'use strict'

angular.module('hazzlepress.common.directives')


.directive('trackVisibilityContainer', function(){
	return{
		restrict: 'A',
		controller: function($scope, $element, $attrs){
			this.element = $element;
		}
	};
})