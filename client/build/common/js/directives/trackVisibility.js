(function(){
	'use strict'


	angular
		.module('hazzlepress.common.directives')
		.directive('trackVisibility', trackVisibility)



	trackVisibility.$inject = ['$window', '$document']

	function trackVisibility($window, $document){
		var registered      = [];
		var scrollContainer = $window;
		// var scrollContainer = document.querySelector('.stream');

		angular.element(scrollContainer).on('DOMContentLoaded load resize scroll', function () {
			angular.forEach(registered, function (item) {
				item[1](isVisible(item[0]));
			});
		});

		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				scope.isVisible = false;

				register(element[0], function(isVisible){
					scope.$apply(function(){
						scope.isVisible = isVisible;
					});
				});

				scope.$on('$destroy', function(){
					deregister(element);
				});
			}
		}

		function register(element, fn) {
			registered.push([element, fn]);
		}

		function deregister(element) {
			registered.every(function (item, key) {
				if(item[0] === element){
					registered.splice(key, 1);
					return;
				}
			});
		}

		function isVisible(el) {
			var rect = el.getBoundingClientRect();
			var clw = (scrollContainer.innerWidth  || $document.documentElement.clientWidth)
			var clh = (scrollContainer.innerHeight || $document.documentElement.clientHeight)

			// checks if element is fully visible
			// return (rect.top >= 0 && rect.bottom <= clh) && (rect.left >= 0 && rect.right <= clw);

			// checks if part of element is visible
			return (rect.left <= clw && 0 <= rect.right && rect.top <= clh && 0 <= rect.bottom);

		}
	}

})()
