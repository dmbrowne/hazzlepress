(function(){
	'use strict'

	angular.module('hazzlepress.common.directives')
		.directive('hzpPost', hzpPost)

	function hzpPost(){
		return{
			restrict: 'E',
			templateUrl: 'common/templates/post.tpl.html'
		}
	}
})()
