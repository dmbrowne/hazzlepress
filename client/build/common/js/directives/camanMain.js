'use strict'

angular
	.module('hazzlepress.common.directives')
	.directive('hzpCaman', hzpCaman)

hzpCaman.$inject = ['$timeout']

function hzpCaman($timeout){
	return {
		require: ['^?hzpAddMediaSelect', '?ngModel'],
		restrict: 'EA',
		controllerAs: 'camanCntrlr',
		bindToController: true,
		controller: function($scope, $element, $attrs){
			var self = this;

			this.presetFilters = [
				{ name: 'vintage'},
				{ name: 'lomo'},
				{ name: 'clarity'},
				{ name: 'sinCity'},
				{ name: 'sunrise'},
				{ name: 'crossProcess'},
				{ name: 'orangePeel'},
				{ name: 'love'}
			];

			this.setCanvasEl = function(el){
				self.camanCanvas = el;
			};

			this.addFilter = function(filter){
				self.camanCanvas.revert(false);

				if(filter === 'normal'){
					self.camanCanvas.render();
				}else{
					self.camanCanvas[filter]();
					self.camanCanvas.render();
				}
			};

			this.saveImageAsBase64 = function(){
				self.camanCanvas.render(function() {
					var base64Image = this.toBase64('jpeg');
					$scope.setMediaItem(base64Image);
				});

				self.finish = true;
			};
		},
		link: function(scope, element, attrs, controllers){
			var hzpAddMediaCntrlr = controllers[0]
			var ngModelCtrlr      = controllers[1]

			scope.setMediaItem = function(b64string){
				if(hzpAddMediaCntrlr !== null)
					hzpAddMediaCntrlr.setMediaItem(b64string)
				else{
					ngModelCtrlr.$setViewValue(b64string)
					ngModelCtrlr.$render()
				}

			}
		}
	};
}
