'use strict';

angular.module('hazzlepress.api', [])

.factory('CommentsApi', ['$resource', function ($resource){
	return {
		post: function(){
			return $resource('http://localhost:3000/api/v1/comments/post/:post_id', {},{
				get: { method: 'POST', isArray: true }
			});
		},
		user: function(){
			return $resource('http://localhost:3000/api/v1/comments/user/:user_id',{},{
				get: { method: 'POST', isArray: true }
			});
		}
	};
}])


.factory('MediaTypesApi', ['$resource', function ($resource){
	return{
		getAll: function(){
			return $resource('http://localhost:3000/api/v1/media_types');
		}
	};
}]);
