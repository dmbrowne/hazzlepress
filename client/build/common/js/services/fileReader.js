'use strict';

angular
	.module('hazzlepress.common.services')
		.factory("fileReaderAPI", ["$q", function ($q) {
			var fileReadData = {
				progress: {
					total: 0,
					loaded: 0,
					percent: 0
				},
				complete: false,
				error: null
			};

			var fileReaderAPI = {
				onComplete: function(deferred, filereaderobj) {
					return function (evt) {
						fileReadData.complete = true;
						deferred.resolve(filereaderobj.result);
					};
				},

				onError: function (deferred){
					return function (evt){
						switch(evt.target.error.code) {
							case evt.target.error.NOT_FOUND_ERR:
								fileReadData.error = 'File Not Found!';
								deferred.reject('File Not Found!');
							break;
							case evt.target.error.NOT_READABLE_ERR:
								fileReadData.error = 'File is not readable';
								deferred.reject('File is not readable');
							break;
							case evt.target.error.ABORT_ERR:
								fileReadData.error = 'Aborted';
								deferred.reject();
							break;
							default:
								fileReadData.error = 'An error occurred reading this file.';
								deferred.reject('An error occurred reading this file.');
						}
					};
				},

				onProgress: function(deferred){
					return function (evt) {
						fileReadData.progress.total   = evt.total;
						fileReadData.progress.loaded  = evt.loaded;
						fileReadData.progress.percent = Math.round((evt.loaded / evt.total) * 100);
						deferred.notify(fileReadData.progress.percent);
					};
				},

				fileReaderInit: function(file) {
					var deferred = $q.defer();

					// the html5 filereader api
					var reader = new FileReader();

					reader.onload     = fileReaderAPI.onComplete(deferred, reader);
					reader.onerror    = fileReaderAPI.onError(deferred);
					reader.onprogress = fileReaderAPI.onProgress(deferred);

					// start readingsetupFileReader
					reader.readAsDataURL(file);

					return deferred.promise;
				},

				readData: function (file, scope) {
					var deferred = $q.defer();

					fileReaderAPI.fileReaderInit(file).then(
						function(fileSrc){ //success
							deferred.resolve(fileSrc);
						},
						function(errorMessage){ // error
							deferred.reject(errorMessage);
						},
						function(percentComplete){ // notify
							deferred.notify(percentComplete);
						})
						.catch(function(err){
							deferred.reject(err);
						});

					return deferred.promise;
				}
			};

			return fileReaderAPI;
		}])
