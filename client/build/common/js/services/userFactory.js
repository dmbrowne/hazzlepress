'use strict';

angular
	.module('hazzlepress.common.services')
	.factory('UserFactory', UserFactory)

UserFactory.$inject = ['UserApi', '$q', 'locker']

function UserFactory(UserApi, $q, locker){
	var userObj = {};
	var userfactory;
	var returnUser;

	userfactory = {
		addUser: addUser,
		returnUser: getUser,
		logout: emptySavedUserObj,
		login: getTokenAndSetUser,
		updateProfile: updateProfile,
		saveUserStage: addUserToStorage,
		getProfileByUserId: getProfileByUserId,
		getProfileByUsername: getProfileByUsername
	};

	return userfactory;

	function addUserToStorage(){
		locker.driver('session').put('userObj', userObj);
	}

	function getUser(){
		// if user have been saved to session then return that user.
		// if not a blank user object will be saved to session, however depending
		// on whether 'rembemver me' is set when logging in, the userObj won't be
		// saved to session storage
		returnUser = locker.driver('session').get('userObj', userObj);

		return returnUser;
	}

	function getTokenAndSetUser(userCredentials){
		/*userCredentials ={
			username: user, <-- provide either email or username
			email: email,   <-- provide either email or username
			password: pass,
			remember: true/false
		}*/
		var deferred = $q.defer();

		var rememberMe = userCredentials.remember;
		var jsonEncodedLogin = JSON.stringify(userCredentials);

		UserApi.getToken().obtain({}, jsonEncodedLogin).$promise
			.then(
				function(userToken){
					userObj.token = userToken.token;
					if(rememberMe) userfactory.saveUserStage();

					UserApi.sessionUser(userObj.token).get_token_user().$promise.then(
						function(user){
							userObj.user = user;
							if(rememberMe) userfactory.saveUserStage();

							deferred.resolve(userObj);
						},
						function(res){
							deferred.reject(res);
						}
					);
				},
				function(res){
					deferred.reject(res);
				}
			);

		return deferred.promise;
	}

	function addUser(userCred){
		var deferred = $q.defer();
		var email    = userCred.email;
		var password = userCred.password;

		var newUserRequestData = JSON.stringify({
			email: email,
			password: password
		});

		UserApi.create().save({}, newUserRequestData)
			.$promise.then(function(newUser){
				deferred.resolve(newUser);
			});

		return deferred.promise;
	}

	function emptySavedUserObj(){
		var i;
		for(i in userObj) {
			delete userObj[i] ;
		}
		locker.driver('session').forget('userObj');
	}

	function getProfileByUserId(userid){
		return UserApi.userWithProfile().get({user_id: userid}).$promise
	}

	function getProfileByUsername(username){
		return UserApi.userWithProfile().get({username: username}).$promise;
	}

	function updateProfile(userid, userprofileobj){
		var deferred = $q.defer();
		var jsonifiedObject = JSON.stringify(userprofileobj);
		UserApi.userWithProfile().update({user_id: userid}, jsonifiedObject)
			.$promise.then(
				function(userProfile){
					deferred.resolve(userProfile);
				}, function(err){
					deferred.reject(err);
				}
			);

		return deferred.promise;
	}
}
