'use strict';

angular
	.module('hazzlepress.common.services')
		.factory('PostsFactory', PostsFactory)

PostsFactory.$inject = ['$q', '_', 'PostsApi', 'UserFactory']

function PostsFactory($q, _, PostsApi, UserFactory){
	var postfactory = {};

	postfactory.getAll         = getAllPosts;
	postfactory.getSinglePost  = getSinglePost;
	postfactory.create         = createPost;
	postfactory.update         = updatePost;
	postfactory.byUser         = getPostsByUserid;
	postfactory.delete         = deletePostByPostId;
	postfactory.deleteMultiple = deleteMultiplePostsByPostIds;
	postfactory.search         = searchForPostsWithTerms;
	postfactory.searchHashtags = searchForPostsByTags;

	return postfactory;

	function getAllPosts(options){
		var deferred            = $q.defer();
		var postRetrivalOptions = {};

		if(options){
			if(options.page_number)
				postRetrivalOptions.page_number = options.page_number;

			if(options.per_page)
				postRetrivalOptions.per_page = options.per_page;

			if(!_.isEmpty(postRetrivalOptions))
				var postRetrival = JSON.stringify(postRetrivalOptions);
		}

		PostsApi.getAll()
			.get({}, postRetrivalOptions)
			.$promise.then(deferred.resolve);

		return deferred.promise;
	}

	function getSinglePost(postid){
		var deferred = $q.defer();

		PostsApi.single()
			.get({post_id: postid})
			.$promise.then(deferred.resolve);

		return deferred.promise;
	}

	function createPost(data, token){
		var deferred = $q.defer();
		// get token manually if not passed as argument
		token = token || UserFactory.returnUser.token;
		// exit if user is not logged in/ cant get token from userFactory
		if(token === undefined || token === null)
			deferred.reject("did not recieve token, make sure you're logged in");

		var jsonifiedData = JSON.stringify(data);

		PostsApi.create(token).save({}, jsonifiedData).$promise.then(
			deferred.resolve,
			deferred.reject,
			deferred.notify('creating post')
		);

		return deferred.promise;
	}

	function updatePost(postid, dataObj, token){
		// get token manually if not passed as argument
		token = token || UserFactory.returnUser.token;
		// exit if user is not logged in/ cant get token from userFactory
		if(token === undefined || token === null)
			$q.reject("did not recieve token, make sure you're logged in");

		var jsonifiedData = JSON.stringify(dataObj);

		return PostsApi.single(token).update({post_id: postid}, jsonifiedData).$promise;
	}

	function getPostsByUserid(userid, options){
		var postRetrivalOptions = {};

		if(!userid)
			$q.reject('no user id recieved');

		if(!_.isEmpty(options))
			postRetrivalOptions = JSON.stringify(options);

		return PostsApi.getByUser().obtain({user_id: userid}, postRetrivalOptions);
	}

	function deletePostByPostId(postid, token){
		token = token  || UserFactory.returnUser().token;
		return PostsApi.single(token).remove({ post_id: postid }).$promise;
	}

	function deleteMultiplePostsByPostIds(postids, token){
		token = token  || UserFactory.returnUser().token;

		return PostsApi.multiDelete(token).delete({},
			JSON.stringify({post_ids: postids})
		).$promise;
	}

	function searchForPostsWithTerms(searchterms, options){
		var searchRequestObj = {
			search_terms: searchterms
		};

		if(options.page_number)
			searchRequestObj.page_number = options.page_number;

		if(options.per_page)
			searchRequestObj.per_page = options.per_page;

		return PostsApi.searchall().obtain({}, searchRequestObj);
	}

	function searchForPostsByTags(searchterms, options){
		var searchRequestObj = {
			tags: searchterms
		};

		if(options.page_number)
			searchRequestObj.page_number = options.page_number;

		if(options.per_page)
			searchRequestObj.per_page = options.per_page;

		return PostsApi.searchtags().obtain({}, searchRequestObj);
	}
}