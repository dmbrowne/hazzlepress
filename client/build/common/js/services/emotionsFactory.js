'use strict';

angular
	.module('hazzlepress.common.services')
		.factory('EmotionsFactory', EmotionsFactory)

EmotionsFactory.$inject = ['EmotionsApi', '$q', '_']

function EmotionsFactory(EmotionsAPI, $q, _){
	var self = this;
	this.emotionsModel = {
		emotions: []
	}

	var gettingEmotions = false;

	return {
		getEmotions: function(){
			var deferred = $q.defer()

			if(self.emotionsModel.emotions.length){
				deferred.resolve(self.emotionsModel)

			}else if( gettingEmotions !== false ){
				gettingEmotions.then(
					resolveEmotions,
					function(err){
						gettingEmotions = false
						deferred.reject(err)
					}
				)

			}else{
				gettingEmotions = EmotionsAPI.getAll().query().$promise

				gettingEmotions.then(
					resolveEmotions,
					function(err){
						gettingEmotions = false
						deferred.reject(err)
					}
				)
				.catch(deferred.reject)
			}

			function resolveEmotions(emos){
				angular.forEach(emos, function(emo){
					if ( _.findIndex(self.emotionsModel.emotions, {id: emo.id}) < 0 ) self.emotionsModel.emotions.push(emo)
				})

				gettingEmotions = false
				deferred.resolve(self.emotionsModel)
			}

			return deferred.promise
		}
	}
}
