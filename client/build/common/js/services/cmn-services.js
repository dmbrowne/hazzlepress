'use strict';

angular.module('hazzlepress.common.services', ['angular-locker'])

.factory('_', ['$window', function($window){
	var _ = $window._;
	delete($window._);
	return(_);
}])
