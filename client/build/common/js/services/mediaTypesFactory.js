'use strict';

angular
	.module('hazzlepress.common.services')
		.factory('MediaTypesFactory', MediaTypeFactory)

MediaTypeFactory.$inject = ['MediaTypesApi', '$q', '_']

function MediaTypeFactory(MediaTypesApi, $q, _){
	var mediatypes = {
		mediaTypeNames: [],
		nameIndexed: {},
		idIndexed: {},
		raw: []
	};

	return {
		getMediatypes: function(){
			return mediatypes;
		},

		downloadMediaTypes: function(){
			var deferred = $q.defer();

			MediaTypesApi.getAll().query().$promise.then(function(mediatypeModel){
				mediatypes.raw = mediatypeModel;

				_.forEach(mediatypeModel, function(val,key){

					mediatypes.mediaTypeNames.push(val.emotion_name);
					mediatypes.nameIndexed[val.emotion_name] = val;
					mediatypes.idIndexed[ val.id.toString()] = val;

					deferred.resolve(mediatypes);

				});
			});

			return deferred.promise;
		}
	};
}
