HazzlePress
===========
a place to express yourself...


### Dependencies
- Node JS v0.10.x (including npm)
- Postgresql


### Using Vagrant

1 Place this project folder into an enclosing folder (e.g. named `hazzlepress_project`), then copy and rename `Vagrant_sample` to `Vagrantfile` and place `Vagrantfile` in the root of the enclosing folder. using the foldername given in the example you should end up with a project structure like so:
```
    hazzlepress_project/
      |- hazzlepress/
      | |- <project files>
```
2 Once the project structure is setup navigate to the enclosing folder root and run `vagrant up`. this will install ubuntu 14.04 LTS with GIT, NVM (node version manager) and postgresql


### Setting up postgresql
Make sure postgres is installed and the service is running before following these steps. (run `service postgresql start` to start the service)
when installing postgresql a shell user (not local user) is auto created with username `postgres`. 


1 SU into the `postgres` user account by running 
```
$ sudo -i -u postgres
```

2 create a new DB user for this project using the interactive shell script available to the postgres user
```
$ createuser hazzlepress
```

3 create a new DB for this project using the script available to the postgres user
```
$ createdb hazzlepress
```

4 set a password for the DB root user (aka postgres user)
```
log into psql (the settings are defaulted to use use the current logged in ubuntu user as the username to connect to postgres with, _WE WILL CHANGE THIS LATER_) *
$ psql

change password for postgres user
postgres=# \password postgres

enter new password twice
postgres=# postgres
postgres=# postgres
```

5 set a password for the hazzlepress user (whilst still logged in as postgres and connected to postgresql shell as postgres)
```
change password for hazzlepress user
postgres=# \password hazzlepress

enter new password twice
postgres=# hazzlepress
postgres=# hazzlepress
postgres=# \q
```

6 set hazzlepress as database owner for hazzlepress database (whilst still logged in as postgres)
```
connect to hazzlepress DB as postgres user
$ psql -U postgres -d hazzlepress

change owner to hazzlepress
hazzlepress=# alter schema public owner to hazzlepress;

hazzlepress=#\q
```

7 Finally, change the authentication method from localuser (peer) to user/password (md5)
```
log out of postgres shell user
$ exit

open postgresql settings
$ sudo nano /etc/postgresql/<postgres version>/main/pg_hba.conf

change peer authentication to md5
change line:
________________________________________________________________________
| local |  all      |       postgres            |                peer  |
------------------------------------------------------------------------
to:
________________________________________________________________________
| local |  all      |       postgres            |                md5   |
------------------------------------------------------------------------

then save and close
ctrl + x
y to save

restart postgres
$ sudo service postgresql restart
```


### Installing the project

> if using NVM run `nvm use` in the root folder to use the node version required for the project

1 Once the dependencies have been satisfied you need to install the following three node modules globally
```
$ npm install -g bower
$ npm install -g gulp
$ npm install -g sequelize-cli
```

2 Install remaining modules for project
```
$ npm install
```

3 git assume unchanged the connection details
```
$ git update-index --assume-unchanged server/config/config.json
```

4 Follow [the database readme](db_readme.md) to install migrations and db seeds


### Build and Development
- To build/compile `gulp dev`
- To start the server `gulp node` and visit http://localhost:3000 (this auto refreshes the servcer (not browser) upon changes made to files)
- To compile for minification and production `gulp` (minified output will be in `/client/bin`)

Please follow the [git commit guide](commit_guidelines.md) when commit to the project :D


### Structure
The project is split into 2 main folders `/client` and `/server`.

```
hazzlepress/
  |- client/
  |  |- build/
  |  |  |- app
  |  |  |  |- <app logic>
  |  |  |- assets/
  |  |  |  |- <static files>
  |  |  |- common/
  |  |  |  |- <reusable code>
  |  |  |- scss/
  |  |  |  |- screen.scss
  |  |- public/(auto generated upon gulp task)
  |  |  |- <files run by app when in use>
  |
  |- server/
  |  |- config/
  |  |  |- <database and user credentials>
  |  |- controllers/
  |  |  |- <logic/functions for each route>
  |  |- karma/
  |  |  |- <test files>
  |  |- migrations/
  |  |  |- <files to create schema relations/tables>
  |  |- models/
  |  |  |- <ORM object representation of relational/table structure>
  |  |- routes/
  |  |  |- <api routes to manage link to controllers>
  |  |- seeds/
  |  |  |- <files to add sample data upon project creation>
  |  |- storage/
  |  |  |- <asset files>
  |  |- views/
  |  |  |- <NOT USED but required by expressJS>
  |- vendor/
  |  |- downloaded/
  |  |- bower_components/

  |- .bowerrc
  |- .nvmrc
  |- .sequelizerc
  |- .gitignore
  |- bower.json
  |- package.json
  |- build.config.js
  |- gulpfile.js
  |- app.js
  |- index.js
```
