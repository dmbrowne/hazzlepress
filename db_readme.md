Database setup
==============
> `#/` refers the project root. not the enclosing folder root, nor the system root

> before doing any of the following ensure the connection details are correct `#/server/config/config.json`.

### Create relations (tables)
1. open `#/.sequelizerc`

2. set migrations path to 'migrations'.
```
# set migrations path to 'sever', 'mirgations'
module.exports = {
    'config':          path.resolve('server', 'config', 'config.json'),
    'migrations-path': path.resolve('server', 'migrations')
}
```

3. run `sequelize db:migrate`
(if this step fails, make dure the postgres service is running and that your connection details in config.json are correct. Also make sure that npm module `sequelize-cli` in installed gloablly)


### Add table data (seeds)
1. open `#/.sequelizerc`

2. set migrations path to 'seeds'.
```
# set migrations path to 'sever', 'seeds'
module.exports = {
    'config':          path.resolve('server', 'config', 'config.json'),
    'migrations-path': path.resolve('server', 'seeds')
}
```

3. run `sequelize db:migrate`
(if this step fails, make dure the postgres service is running and that your connection details in config.json are correct. Also make sure that npm module `sequelize-cli` in installed gloablly)
