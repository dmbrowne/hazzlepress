var gulp            = require('gulp'),
	// uglify          = require('gulp-uglifyjs'),
	html2js         = require('gulp-html2js'),
	concat          = require('gulp-concat'),
	sass            = require('gulp-sass'),
	jshint          = require('gulp-jshint'),
	jshintStylish   = require('jshint-stylish'),
	rename          = require("gulp-rename"),
	inject          = require("gulp-inject"),
	watch           = require('gulp-watch'),
	clean           = require('gulp-clean'),
	plumber         = require('gulp-plumber'),
	nodemon         = require('gulp-nodemon');

var userConfig = require( './build.config.js' ); /* Load in our build configuration file. */
var packageFile = require('./package.json'); /* Load in our build packagejson file. */

/**
 * =================
 *  GULP DEV & PROD TASKS
 * =================
 * Tasks for dev compiling and prod compiling
 * html2js - concating templates all .tpl.html files and storing as angular cache
 * jshint/lint - both application js  and this gulpfile.js
 *
 */

gulp.task('html2js-app', function() {
	var stream =
		gulp.src(userConfig.app_files.atpl)
			.pipe(html2js({
				outputModuleName: 'app-templates',
				useStrict: false,
				base: userConfig.app_files.appbase
			}))
			.pipe(concat('templates-app.tpl.js'))
			.pipe( gulp.dest(userConfig.build_dir) );

	return stream;
});

gulp.task('html2js-common', function() {
	var stream =
		gulp.src(userConfig.app_files.ctpl)
			.pipe(html2js({
				outputModuleName: 'common-templates',
				useStrict: false,
				base: userConfig.dev_dir
			}))
			.pipe( concat('templates-common.tpl.js') )
			.pipe( gulp.dest(userConfig.build_dir) );

	return stream;
});

gulp.task('lint-appfiles', function() {
	var stream =
		gulp.src(userConfig.app_files.js)
			.pipe(jshint({
					strict: false,
					devel: false,
					asi: true
				}))
			.pipe(jshint.reporter(jshintStylish));

	return stream;
});

gulp.task('lint-gulpfile', function() {
	gulp.src('gulpfile.js')
		.pipe(jshint({
				strict: false,
				devel: false,
				asi: true
			}))
		.pipe(jshint.reporter(jshintStylish));
});

// Copy over all custom js to the compile directory
gulp.task('build-appjs', ['lint-appfiles'], function() {
	var stream = gulp.src(userConfig.app_files.js)
		.pipe(gulp.dest(userConfig.build_dir));
	return stream;
});

gulp.task('build-vendorjs', function() {
	var stream = gulp.src(userConfig.vendor_files.js)
		.pipe(gulp.dest(userConfig.build_dir));
	return stream;
});

gulp.task('copy-app-assets', function() {
	var stream = gulp.src(userConfig.app_files.assets)
		.pipe(gulp.dest(userConfig.build_dir + '/assets/'));
	return stream;
});

gulp.task('copy-vendor-assets', function() {
	var stream = gulp.src(userConfig.vendor_files.assets)
		.pipe(gulp.dest(userConfig.build_dir + '/assets'));
	return stream;
});

gulp.task('copy-vendor-css', function() {
	if(userConfig.vendor_files.css.length > 0){
		var stream = gulp.src(userConfig.vendor_files.css)
			.pipe(gulp.dest(userConfig.build_dir + '/assets/css'));
		return stream;
	}
});


gulp.task('index', ['sass', 'build-appjs', 'html2js-app', 'html2js-common', 'copy-app-assets', 'copy-vendor-assets'],
	function () {
		gulp.src(userConfig.dev_dir + '/index.html')
			.pipe(inject(gulp.src('assets/css/*',{
					cwd: userConfig.build_dir,
					read: false
				}), {
					addRootSlash: true,
					starttag: '<!-- inject:head:css -->'
				}
			))
			// .pipe(inject(gulp.src('modernizr-2.6.2.min.js',{
			// 		cwd: userConfig.build_dir,
			// 		read: false
			// 	}), {
			// 		addRootSlash: true,
			// 		starttag: '<!-- inject:head:js -->'
			// 	}
			// ))
			.pipe(inject(gulp.src([
					// '!modernizr-2.6.2.min.js',
					'jquery.min.js',
					'moment.min.js',
					'angular.js',
					'angular*.js',
					'*.js',
					'**/*.js'
				],
				{
					cwd: userConfig.build_dir,
					read: false
				}),
				{addRootSlash: false }
			))
			.pipe(gulp.dest(userConfig.build_dir));
	}
);


/**
 * =================
 *  GULP DEV TASKS
 * =================
 * Tasks only for dev compilation
 * Sass - compile sass with compass ruby but not minify it
 *
 */
gulp.task('sass', function() {
	return gulp.src(userConfig.app_files.scss)
		.pipe(plumber())
        .pipe(sass({
        	// sync: true                        // Uses renderSync instead if 'true'
        	includePaths: [
        		userConfig.dev_dir + '/app',
        		userConfig.dev_dir + '/common',
        		'vendor/downloaded',
        		'vendor/bower_components'
        	],
        	imagePath: 'public/img',             // A String that represents the public image path when using the image-url() function
    		outputStyle: 'compact'
        }))
        .pipe(gulp.dest(userConfig.build_dir + '/css'));
});

gulp.task('serve', function(){
	nodemon({
		script: 'index.js',
		ext: 'html js css png jpg',
		watch: ['client/public']
	});
});

/**
 * =================
 *  GULP PROD TASKS
 * =================
 * Tasks only for production compilation
 * Sass - compile sass with compass ruby and also concatenate it
 *
 */
 /* Production Tasks */
gulp.task('compile-vendorjs', ['dev'], function() {
	var stream = gulp.src(userConfig.vendor_files.js)
		.pipe(concat(packageFile.name + '.js'))
		.pipe(gulp.dest(userConfig.compile_dir + '/assets'));
	return stream;
});

gulp.task('compile-appjs', ['compile-vendorjs'], function() {
	var stream = gulp.src([
			userConfig.compile_dir + '/assets/'+ packageFile.name + '.js',
			userConfig.build_dir + '/app/**/*.js',
			userConfig.build_dir + '/common/**/*.js',
			userConfig.build_dir + '/**/*.tpl.js',
		])
		.pipe(concat(packageFile.name + '.js'))
		.pipe(gulp.dest(userConfig.compile_dir + '/assets'));

	return stream;
});

gulp.task('minify-js', ['compile-appjs'], function() {
	gulp.src(userConfig.compile_dir + '/assets/' + packageFile.name + '.js')
		.pipe(ngAnnotate())
		.pipe(uglify({mangle: true}))
		.pipe(gulp.dest(userConfig.compile_dir + '/assets'));
});


gulp.task('sass-prod', function() {
	return gulp.src(userConfig.app_files.scss)
		.pipe(plumber())
        .pipe(sass({
        	// sync: true             // Uses renderSync instead if 'true'
        	includePaths: [
        		userConfig.dev_dir + '/app',
        		userConfig.dev_dir + '/common',
        		'vendor/downloaded/',
        		'vendor/bower_components/'
        	],
        	imagePath: 'public/assets/img',
    		outputStyle: 'compressed'
        }))
        .pipe(gulp.dest(userConfig.compile_dir + '/css'));
});


gulp.task('compile-assets', ['copy-app-assets'], function() {
	var stream = gulp.src([userConfig.build_dir + '/assets/*', userConfig.build_dir + '/assets/**/*'])
		.pipe(gulp.dest(userConfig.compile_dir + '/assets'));
	return stream;
});

gulp.task('index-prod', ['compile-assets', 'sass-prod'], function () {
	gulp.src(userConfig.dev_dir + '/index.html')
		.pipe(inject(
			gulp.src(
				['**/*.js', '**/*.css'],
				{
					cwd: userConfig.compile_dir,
					read: false
				}
			),
			{addRootSlash: true }
		))
		.pipe(gulp.dest(userConfig.compile_dir));
});

gulp.task('dev', [
	'sass',
	'html2js-app',
	'html2js-common',
	'lint-gulpfile',
	'lint-appfiles',
	'build-appjs',
	'build-vendorjs',
	'copy-app-assets',
	'copy-vendor-assets',
	'copy-vendor-css',
	'index'
]);

gulp.task('default', [
	'dev',
	'compile-vendorjs',
	'compile-appjs',
	'minify-js',
	'sass-prod',
	'index-prod'
]);

//Rerun the task when a file changes
gulp.task('watch', function() {

	gulp.watch('gulpfile.js', ['lint-gulpfile']);

	gulp.watch(userConfig.app_files.js, ['lint-appfiles', 'build-appjs', 'index']);

	gulp.watch( [userConfig.dev_dir + '/assets/**', userConfig.vendor_files.assets], ['copy-app-assets', 'copy-vendor-assets', 'index']);

	gulp.watch( userConfig.app_files.html, ['index']);

	gulp.watch( [userConfig.app_files.atpl, userConfig.app_files.ctpl], ['html2js-app', 'html2js-common']);

	gulp.watch( [userConfig.dev_dir + '/*.scss',userConfig.dev_dir + '/**/*.scss'], ['sass', 'index']);

});

gulp.task('node', ['dev','serve', 'watch']);
