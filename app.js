var express      = require('express');
var path         = require('path');
var favicon      = require('serve-favicon');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var busboy       = require('connect-busboy');
var env          = process.env.NODE_ENV || "development";
var config       = require(__dirname + '/server/config/config.json')[env];

var routes       = require('./server/routes/routes');
var app          = express();

app.set('views', path.join(__dirname, 'server/views')); // specify the views directory (only using for error page as angular is habdling the client routing)
app.set('view engine', 'jade');                         // register the template engine
app.set('models', require('./server/models'));

// uncomment after placing your favicon in /public
// app.use(favicon(__dirname + '/public/assets/raised-fist-32.png'));
app.use(busboy({ immediate: true }));
if(process.env.NODE_ENV !== 'test') app.use(logger('dev'));
app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
app.use(cookieParser());
app.use(express.static(__dirname + '/client/public'));
app.use(express.static(__dirname + '/server/storage'));



// ROUTES FOR OUR API
app.use('/api/v1', routes);


// Angular JS handling routing on the front end
// so any requests that don't match already defined routes above - forward to front end index.html to deal with
app.get('*', function(req, res){
	res.sendFile('index.html', { root: __dirname + '/client/public' });
});


/* ========================
 * Error handlers
 * ======================== */

// TODO: Refactor - 404 handler may not be needed if angular is handling routes
// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
})

// 500 error handler,
// If developement environment - will print stacktrace
app.use(function(err, req, res, next) {
	var returnErr = app.get('env') === 'development' ? err : {}
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: returnErr
	});
})



module.exports = app
