"use strict";

// Get Models
var models  = require('../models');

module.exports = {
	up: function(migration, DataTypes, done) {
		// http://www.materialpalette.com/
		var emotions = [
			{
				emotion_name: "LOL",
				slug: "lol",
				is_voteable: true
			},     // yellow & yellow

			{
				emotion_name: "WTF",
				slug: "wtf",
				is_voteable: true
			},     // purple and amber

			{
				emotion_name: "Love",
				slug: "love",
				is_voteable: true
			},     // pink & red

			{
				emotion_name: "Like",
				slug: "like",
				is_voteable: true
			},     // light-blue & indigo

			{
				emotion_name: "Hate",
				slug: "hate",
				is_voteable: true
			},     // brown & grey

			{
				emotion_name: "Agree",
				slug: "agree",
				is_voteable: true
			},    // brown & grey

			{
				emotion_name: "Disagree",
				slug: "disagree",
				is_voteable: true
			}, // brown & grey

			{
				emotion_name: "Happy",
				slug: "happy"
			},                   // green & lime
			{
				emotion_name: "Mellow/ Indifferent",
				slug: "indifferent"
			},         // deep purple or amber & yellow

			{
				emotion_name: "Sad",
				slug: "sad"
			},                         // blue grey & brown
			{
				emotion_name: "Disgust",
				slug: 'disgust'
			},                     // grey & brown
			{
				emotion_name: "Anger",
				slug: "anger"
			}                        // red & brown
		];

		models.Emotion.bulkCreate(emotions)
			.then(function(){
				done();
			})
			.catch(console.log);
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('TRUNCATE TABLE emotions CASCADE')
			.then(function(){
				console.log('truncated emotions \n');
				done();
			});
	}
};
