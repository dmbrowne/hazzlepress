"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration
			.createTable('tags',{
				id:{
					type: DataTypes.STRING(24),
					primaryKey: true
				},

				name:{
					type: DataTypes.STRING(160), //limit of description in post, so tag can't possbily be longer than that
					unique: true
				}
			})
			.then(function(){
				migration.sequelize.query('ALTER TABLE tags ALTER COLUMN id SET DEFAULT mongodb_object_id()')
					.then(function(){
						done()
					})
					.catch(function(err){
						console.log(err);
						done(err);
					});
			})
			.catch(function(err){
				console.log(err);
				done(err);
			});
	},

	down: function(migration, DataTypes, done) {
		// add reverting commands here, calling 'done' when finished
		migration.dropTable('tags').complete(done);
	}
};
