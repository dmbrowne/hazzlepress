"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.sequelize.query('alter table posts drop column emotion_id cascade')
			.then(function(){
				done();
			});
	},

	down: function(migration, DataTypes, done) {
		// add reverting commands here, calling 'done' when finished
		// CANT BE REVERSED :(
		done();
	}
};
