"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.createTable("post_emotion_through", {

			emotion_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				references: 'emotions',
				referencesKey: 'id'
			},

			post_id: {
				type: DataTypes.STRING(24),
				references: 'posts',
				referencesKey: 'id'
			}

		})
		.then(function(){
			done();
		})
		.catch(function(err){
			console.log(err);
			done(err);
		});
	},

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
		migration
			.sequelize.query('DROP TABLE post_emotion_through CASCADE')
			.then(function(){
					done()
			});
	}
};
