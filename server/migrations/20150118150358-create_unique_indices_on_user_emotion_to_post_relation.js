"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.addIndex('post_feedback_emotions', ['post_id', 'user_id', 'emotion_id'], {
				indicesType: 'UNIQUE'
			})
			.then(function(){
				done()
			});
	},

	down: function(migration, DataTypes, done) {
		migration.removeIndex('post_feedback_emotions', ['post_id', 'user_id', 'emotion_id'])
			.then(function(){
				done()
			});
	}
};
