"use strict";
var _       = require('lodash');
var chance  = require('chance').Chance();
var models  = require('../models');
var fs      = require('fs');
var $q      = require('q');

module.exports = {
	up: function(migration, DataTypes, done) {

		$q.all([
			models.PostMedia.findAll(),
			models.Post.findAll({ attributes: ['id']})
		])
		.spread(addPostIdToMediaRow);

		function addPostIdToMediaRow(postmedia, posts){
			var mediaUpdatePromise = [];

			for(var i = 0; i < postmedia.length; i++){
				// get a random post from the posts array
				var randomSelectionNumber = chance.natural({min: 0, max: posts.length});
				var selectedPost = posts[randomSelectionNumber];

				// remove the selected post from array so it can't be chosen again
				posts.splice(randomSelectionNumber, 1);

				mediaUpdatePromise.push(
					postmedia[i].updateAttributes({
						post_id: selectedPost.getDataValue('id')
					})
				);

				if( (i + 1) >= postmedia.length )
					waitForUpdatesThenEnd(mediaUpdatePromise);
			}
		}

		function waitForUpdatesThenEnd(promise){
			$q.all(promise).then(function(result){ console.log('result.length ', result.length); done(); });
		}
	},

	down: function(migration, DataTypes, done) {
		// add reverting commands here, calling 'done' when finished
		migration.sequelize.query('alter table post_media drop column post_id cascade')
			.then(function(){
				migration.addColumn(
					'post_media',
					'post_id',
					{
						type: DataTypes.STRING(24),
						references: 'posts',
						referencesKey: 'id'
					}
				).then(function(){
					done();
				});
			});
	}
};
