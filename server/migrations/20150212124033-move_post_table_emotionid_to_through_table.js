"use strict";

var $q   = require('q');
var _    = require('lodash');
var models  = require('../models');
var chance = require('chance').Chance();

module.exports = {
  up: function(migration, DataTypes, done) {
    // add altering commands here, calling 'done' when finished
    //done();
    $q.all([
        models.Post.findAll(),
        models.Emotion.findAll()
    ])
    .spread(function(posts, emotions){
            var postids = _.pluck(posts, 'id')
            var emoids = _.pluck(emotions, 'id')

            var postAndEmotionsIds = posts.map(function(post){
                var emotionid = chance.pick(emoids)
                return {
                    post_id: post.id,
                    emotion_id: emotionid
                }
            })

    		models.PostEmotionThrough
    			.bulkCreate(postAndEmotionsIds)
    			.then(function(){
    				done();
    			})
    	})
  },

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    migration.sequelize.query('truncate table post_emotion_through cascade')
    	.then(function(){
    		done();
    	});
  }
};
