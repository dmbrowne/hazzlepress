"use strict";

module.exports = {
  up: function(migration, DataTypes, done) {
    migration.sequelize.query("ALTER TABLE profiles ADD COLUMN profiles_tsv tsvector")
    	.then(function(){
    		migration.sequelize.query(
    		"UPDATE profiles SET profiles_tsv = to_tsvector(coalesce(username,'') || ' ' || coalesce(name,'') || ' ' || coalesce(bio,''))")
    		.then(function(){
    			migration.sequelize.query("CREATE INDEX profiles_textsearch_idx ON profiles USING gin (profiles_tsv)")
						.then(function(){
							done()
    			});
    		})
    	})
    	.catch(function(err){
    		done(err)
    	})
  },

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    migration
    	.removeColumn('profiles', 'profiles_tsv')
    	.complete(done);
  }
};
