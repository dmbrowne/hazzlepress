"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration
			.createTable('posts', {
				id: {
					type: DataTypes.STRING(24),
					allowNull: false,
					primaryKey: true
				},

				user_id:{
					type: DataTypes.STRING(24),
					allowNull: false,
					references: "users",
					referencesKey: "id"
				},

				description: DataTypes.STRING(500),

				emotion_id: {
					type: DataTypes.INTEGER,
					allowNull: true,
					references: "emotions",
					referencesKey: "id"
				},

				created_at: {
					type: DataTypes.DATE,
					allowNull: false,
				},

				updated_at: {
					type: DataTypes.DATE,
					allowNull: false,
					defaultValue: DataTypes.NOW
				},

				deleted_at: {
					type: DataTypes.DATE
				}
			})
			.then(function(){
				migration.sequelize.query('ALTER TABLE posts ALTER COLUMN id SET DEFAULT mongodb_object_id()')
					.then(function(){
						done()
					})
					.catch(function(err){
						console.log(err);
						done(err);
					});
			})
			.catch(function(err){
				console.log(err);
				done(err);
			});
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('DROP TABLE posts CASCADE')
			.then(done)
			.catch(function(err){
				console.error(err);
			});
	}
};
