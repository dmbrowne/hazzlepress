"use strict";

var chance = require('chance').Chance();
var models = require('../models');
var fs     = require('fs');

module.exports = {
	up: function(migration, DataTypes, done) {
		var profiles    = [];
		var rowsToMake  = 20;
		var users       = models.User.findAll();
		var avatarfiles = fs.readdirSync(__dirname + '/../storage/avatars/large');

		chance.mixin({'profileMixin': userprofileChanceMixin});

		users.success(function(allusers){
			allusers.forEach(createProfile);

			models.Profile.bulkCreate(profiles)
				.then(function() {
					done();
				})
				.catch(console.log)
		});

		function createProfile(userInstance, index, fullArray){
			var newProfile     = chance.profileMixin();
			newProfile.user_id = userInstance.getDataValue('id');
			profiles.push(newProfile);
		}

		function getAvatarFromRemainingSet(){
			var profileImgFile = chance.pick(avatarfiles);
			var idxPos    = avatarfiles.indexOf(profileImgFile);
			avatarfiles.splice(idxPos, 1);
			return profileImgFile;
		}

		function userprofileChanceMixin() {
				var firstname = chance.first();
				var lastname = chance.last();
				return {
					user_id: undefined,
					username: (firstname + "_" + lastname + chance.natural({min: 1, max: 20}) ),
					name: firstname + " " + lastname,
					bio: chance.paragraph({sentences: 1}),
					profile_photo: getAvatarFromRemainingSet(),
					website: chance.url(),
					phone: chance.phone(),
					gender: chance.pick(['male', 'female', null]),
					private: chance.bool()
				};
			}
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('TRUNCATE TABLE profiles CASCADE')
				.complete(function(){
					console.log('profiles truncated');
					done();
				});
	}
};
