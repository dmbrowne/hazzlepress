"use strict";

// Load Chance and Instantiate Chance so it can be used
var chance = require('chance').Chance();
// Get Models
var models  = require('../models');

module.exports = {
	up: function(migration, DataTypes, done) {
		var users = [];
		var rowsToMake = 20;

		chance.mixin({
			'userMixin': function() {
				return {
					email: chance.email(),
					password: chance.string()
				};
			}
		});

		for(var i=0; i < rowsToMake; i++){
			users[i] = chance.userMixin();
		};

		models.User
			.bulkCreate(users)
			.then(function() {
				done();
			})
			.catch(function(error) {
				console.log(error);
			});
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('TRUNCATE TABLE users CASCADE')
				.complete(function(){
					console.log('users truncated');
					done();
				});
	}
};
