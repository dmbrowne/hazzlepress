"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.createTable('users', {
			id: {
				type: DataTypes.STRING(24),
				primaryKey: true,
				allowNull: false,
			},

			email: {
				type: DataTypes.STRING(50),
				allowNull: false,
				unique: true
			},

			password: {
				type: DataTypes.STRING,
				allowNull: false
			},

			active: {
				type: DataTypes.BOOLEAN,
				defaultValue: true,
				allowNull: false
			},

			created_at: {
				type: DataTypes.DATE,
				allowNull: false
			},

			updated_at: {
				type: DataTypes.DATE,
				allowNull: false,
				defaultValue: DataTypes.NOW
			},

			deleted_at: {
				type: DataTypes.DATE
			}
		})
		.then(function(){
			migration.sequelize.query('ALTER TABLE users ALTER COLUMN id SET DEFAULT mongodb_object_id()')
				.then(function(){
					done();
				})
				.catch(function(err){
					console.log(err);
					done(err);
				});
		})
		.catch(function(err){
			console.log(err);
			done(err);
		});
},

	down: function(migration, DataTypes, done) {
		migration
			.dropTable('DROP TABLE users CASCADE')
			.then(function(){
				done()
			});
	}
};
