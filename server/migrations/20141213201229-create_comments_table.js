"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration
			.createTable('post_comments', {
				id: {
					type: DataTypes.STRING(24),
					allowNull: false,
					primaryKey: true // index post id so that grabbing all comment via post id is fast
				},

				post_id:{
					type: DataTypes.STRING(24),
					allowNull: false,
					references: "posts",
					referencesKey: "id"
				},

				comment: DataTypes.STRING(500),

				user_id: {
					type: DataTypes.STRING(24),
					references: "users",
					referencesKey: "id"
				},

				created_at: {
					type: DataTypes.DATE,
					allowNull: false,
				},

				updated_at: {
					type: DataTypes.DATE,
					allowNull: false,
					defaultValue: DataTypes.NOW
				},

				deleted_at: DataTypes.DATE

			})
			.complete(function(err){
				migration.sequelize.query('ALTER TABLE post_comments ALTER COLUMN id SET DEFAULT mongodb_object_id()')
					.complete(done);
			});
	},

	down: function(migration, DataTypes, done) {
		migration
			.dropTable('post_comments')
			.complete(done);
	}
};
