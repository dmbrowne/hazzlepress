"use strict";

module.exports = {
  up: function(migration, DataTypes, done) {
    // add altering commands here, calling 'done' when finished
    migration.sequelize.query(
      "CREATE TRIGGER profiles_tsvectorupdate BEFORE INSERT OR UPDATE " +
		    "ON profiles FOR EACH ROW EXECUTE PROCEDURE " +
		    "tsvector_update_trigger( " +
			     "profiles_tsv, 'pg_catalog.english', " +
			     "username, name, bio)"
			 )
    	.then(function(){ done(); })
    	.catch(function(err){
    		console.log(err);
    		done(err)
    	});
  },

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
     migration.sequelize.query("DROP TRIGGER IF EXISTS profiles_tsvectorupdate ON profiles")
     	.then(function(){
     		done();
     	})
     	.catch(function(err){
     		console.log(err);
     		done(err);
     	});
  }
};
