"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.createTable("post_media", {
			id: {
				type: DataTypes.STRING(24),
				primaryKey: true,
				allowNull: false,
			},
			
			location: {
				type: DataTypes.STRING,
				allowNull: false
			},

			media_type_id:{
				type: DataTypes.INTEGER,
				allowNull: false,
				references: 'media_types',
				referencesKey: 'id'
			},
			
			post_id: {
				type: DataTypes.STRING(24),
				references: 'posts',
				referencesKey: 'id'
			},
			
			created_at: {
				type: DataTypes.DATE,
				allowNull: false
			}
		})
		.then(function(){
			migration.sequelize.query('ALTER TABLE post_media ALTER COLUMN id SET DEFAULT mongodb_object_id()')
				.then(function(){
					done();
				})
		})
		.catch(function(err){
			console.log(err);
			done(err);
		});
	},

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
		migration
			.sequelize.query('DROP TABLE post_media CASCADE')
			.then(function(){
					done()
			});
	}
};
