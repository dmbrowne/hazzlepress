"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.sequelize.query("ALTER TABLE posts ADD COLUMN posts_tsv tsvector")
			.then(function(){
				migration.sequelize.query("UPDATE posts SET posts_tsv = to_tsvector(coalesce(description,''))")
				.then(function(){
					migration.sequelize.query("CREATE INDEX posts_ts_idx ON posts USING gin (posts_tsv)")
							.then(function(){
								migration.sequelize.query(
									"CREATE TRIGGER posts_tsvectorupdate BEFORE INSERT OR UPDATE " +
									"ON posts FOR EACH ROW EXECUTE PROCEDURE " +
									"tsvector_update_trigger( " +
									 "posts_tsv, 'pg_catalog.english', " +
									 "description " +
									")"
								).then(function(){
									done();
								});
							});
				})
			})
			.catch(function(err){
				console.log(err)
			});
	},

	down: function(migration, DataTypes, done) {
			migration.sequelize.query("DROP TRIGGER posts_tsvectorupdate ON posts")
				.then(function(){
					migration.sequelize.query("DROP INDEX posts_ts_idx")
						.then(function(){
							migration.sequelize.query("ALTER TABLE posts DROP COLUMN posts_tsv tsvector CASCADE")
								.then(done);
						})
				})
	}
};
