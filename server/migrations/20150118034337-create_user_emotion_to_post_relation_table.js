"use strict";

module.exports = {
  up: function(migration, DataTypes, done) {
    migration.createTable('post_feedback_emotions', {
		id: {
			type: DataTypes.STRING(24),
			primaryKey: true,
			allowNull: false,
		},

		post_id:{
			type: DataTypes.STRING(24),
			allowNull: false,
			references: "posts",
			referencesKey: "id"
		},

		user_id:{
			type: DataTypes.STRING(24),
			allowNull: false,
			references: "users",
			referencesKey: "id"
		},

		emotion_id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: "emotions",
			referencesKey: "id"
		},

		created_at: {
			type: DataTypes.DATE,
			allowNull: false
		},

		updated_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: DataTypes.NOW
		},

		deleted_at: {
			type: DataTypes.DATE
		}
	})
	.then(function(){
		migration.sequelize.query('ALTER TABLE post_feedback_emotions ALTER COLUMN id SET DEFAULT mongodb_object_id()')
			.complete(done);
	});
},

  down: function(migration, DataTypes, done) {
  	migration.dropTable('post_feedback_emotions')
    .then(function(){
    	done();
    });
  }
};
