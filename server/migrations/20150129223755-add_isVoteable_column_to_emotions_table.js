"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {

		migration.addColumn(
			'emotions',         // Table
			'is_voteable',      // Column Name
			 DataTypes.BOOLEAN  // Datatype
		)
		.then(function(){
			done();
		})
		.catch(console.error);

	},

	down: function(migration, DataTypes, done) {
		migration.removeColumn('emotions', 'is_voteable')
			.then(function(){
				done();
			})
			.catch(console.error);
	}
};
