"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.createTable('profiles', {

			id: {
				type: DataTypes.STRING(24),
				allowNull: false,
				primaryKey: true
			},

			user_id: {
				type: DataTypes.STRING(24),
				allowNull: false,
				references: "users",
				referencesKey: "id"
			},

			username: {
				type: DataTypes.STRING(20),
				allowNull: true,
				unique: true
			},

			name: DataTypes.STRING(35),

			bio: DataTypes.STRING(160),

			profile_photo: DataTypes.TEXT,

			website: DataTypes.STRING(60),

			phone: DataTypes.STRING(20),

			gender: {
				type: DataTypes.ENUM,
				values: ['male', 'female'],
				allowNull: true
			},

			private: {
				type: DataTypes.BOOLEAN,
				defaultValue: false
			},

			created_at: {
				type: DataTypes.DATE,
				allowNull: false
			},

			updated_at: {
				type: DataTypes.DATE,
				allowNull: false,
				defaultValue: DataTypes.NOW
			}
		})
		.then(function(){
			migration.sequelize.query('ALTER TABLE profiles ALTER COLUMN id SET DEFAULT mongodb_object_id()')
				.then(function(){
					done()
				})
				.catch(function(err){
					console.log(err);
					done(err);
				});
		})
		.catch(function(err){
			console.log(err);
			done(err);
		});
	},

	down: function(migration, DataTypes, done) {
		// add reverting commands here, calling 'done' when finished
		migration
			.dropTable('profiles')
			.complete(done);
	}
};
