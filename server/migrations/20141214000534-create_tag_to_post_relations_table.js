"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration
			.createTable('post_tag_associations',{

				tag_id:{
					type: DataTypes.STRING(24),
					allowNull: false,
					references: "tags",
					referencesKey: "id"
				},

				post_id:{
					type: DataTypes.STRING(24), //limit of description in post, so tag can't possbily be longer than that
					allowNull: false,
					references: "posts",
					referencesKey: "id"
				}
			})
			.complete(done);
	},

	down: function(migration, DataTypes, done) {
		// add reverting commands here, calling 'done' when finished
		migration.dropTable('post_tag_associations').complete(done);
	}
};
