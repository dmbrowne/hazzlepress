"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration
			.addIndex('post_tag_associations', ['tag_id', 'post_id'], {
				indicesType: 'UNIQUE'
			})
			.complete(done);
	},

	down: function(migration, DataTypes, done) {
		// add reverting commands here, calling 'done' when finished
		migration
			.removeIndex('post_tag_associations', ['tag_id', 'post_id'])
			.complete(done);
	}
};
