"use strict";

module.exports = {
  up: function(migration, DataTypes, done) {
    // add altering commands here, calling 'done' when finished
    migration.createTable('followers',{

    	user_id:{
    		type: DataTypes.STRING(24),
    		allowNull: false,
    		references: 'users',
    		referencesKey: 'id'
    	},

    	follower_id:{
    		type: DataTypes.STRING(24),
    		allowNull: false,
    		references: 'users',
    		referencesKey: 'id'
    	}
    })
    .then(function(){
    		migration.addIndex('followers', ['user_id', 'follower_id'], {
						indicesType: 'UNIQUE'
					})
					.then(function(){
						done()
					})
    })
  },

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    done();
  }
};
