"use strict";

// Get Models
var models  = require('../models');

module.exports = {
	up: function(migration, DataTypes, done) {
		var mediatypes = [
			{ media_type: "text" },
			{ media_type: "video" },
			{ media_type: "image" }
		];

		models.MediaType.bulkCreate(mediatypes)
			.then(function(){
				done();
			})
			.catch(console.log);
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('TRUNCATE TABLE media_types CASCADE')
			.then(function(){
				console.log('truncated media_types \n');
				done();
			});
	}
};
