"use strict";

// Load Chance and Instantiate Chance so it can be used
var chance  = require('chance').Chance();
// Get Models
var models  = require('../models');
var $q      = require('q');

module.exports = {
	up: function(migration, DataTypes, done) {

		getUsersAndPosts().then(function(results){
			var allPostInstances = results.sequelizePosts;
			var allUserInstances = results.sequelizeUsers;

			allPostInstances.forEach(function(postModel, index, originalArray){
				var postId           = postModel.getDataValue('id');
				var numberOfComments = chance.natural({min: 1, max: 70});
				var commentsList     = [];

				for (var i = 0; i < numberOfComments; i++) {
					var randomNum     = getRandomInt(0, allUserInstances.length),
						randomUser    = allUserInstances[randomNum],
						userId        = randomUser.getDataValue('id'),
						noOfSentences = getRandomInt(1, 4);

					commentsList.push({
						comment: chance.paragraph({sentences: noOfSentences}),
						user_id: userId,
						post_id: postId
					})
				};

				models.PostComment.bulkCreate(commentsList)
				.then(function(){
					if((index + 1) === originalArray.length) done();
				})
				.catch(function(err){
					console.error('error on insertion loop' + index + ', error is.... \n', err);
				});
			})
		});

		function getUsersAndPosts(){
			var deferred = $q.defer();
			$q.all([ models.Post.findAll(), models.User.findAll() ])
			.then(function(results){
				deferred.resolve({
					sequelizeUsers: results[1],
					sequelizePosts: results[0],
				});
			});
			return deferred.promise;
		};

		// Returns a random integer between min (included) and max (excluded)
		// Using Math.round() will give you a non-uniform distribution!
		function getRandomInt(min, max) {
			return Math.floor(Math.random() * (max - min)) + min;
		}
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('TRUNCATE TABLE post_comments CASCADE')
			.complete(function(){
				console.log('post_comments truncated');
				done();
			});
	}
};
