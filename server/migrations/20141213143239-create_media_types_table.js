"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.createTable('media_types',{
			id:{
				type: DataTypes.INTEGER,
				autoIncrement: true,
				primaryKey: true
			},

			media_type:{
				type: DataTypes.STRING(30),
				unique: true
			}
		})
		.then(function(){
			done();
		})
		.catch(function(err){
			console.log(err);
			done(err);
		});
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('DROP TABLE media_types CASCADE')
			.then(done)
			.catch(function(err){
				console.error(err);
			});
	}
};
