"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.createTable('emotions',{
			id:{
				type: DataTypes.INTEGER,
				autoIncrement: true,
				primaryKey: true
			},

			emotion_name:{
				type: DataTypes.STRING(30),
				unique: true
			},

			slug:{
				type: DataTypes.STRING(30),
				unique: true
			}
		})
		.then(function(){
			done();
		})
		.catch(function(err){
			console.log(err);
			done(err);
		});
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('DROP TABLE emotions CASCADE')
			.then(done)
			.catch(function(err){
				console.error(err);
			});
	}
};
