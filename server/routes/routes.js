var express     = require('express');
var app         = express();
var router      = express.Router();
var env         = process.env.NODE_ENV || "development";
var config      = require(__dirname + '/../config/config.json')[env];
var controllers = require(__dirname + '/routeControllers');
var expressJwt  = require('express-jwt');



// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging, authenticate user etc.
	// make sure we go to the next routes and don't stop here
	next();
});

router.get('/', function(req, res) {
	res.json({ message: 'hooray! welcome to our api!' });
});



// USER ROUTES
// ----------------------------------------------------

router.route('/users')
	.get(controllers.Users.listAll)
	.post(controllers.Users.create);

router.route('/login')
	.post(controllers.Users.getToken);

router.route('/users/current')
	.get(expressJwt({secret: config.secret}), controllers.Users.getSessionUser);

router.route('/users/:user_id_name')
	.get(controllers.Users.getUser);

router.route('/users/:user_id')
	.put(controllers.Users.updateUser)
	.delete(controllers.Users.deleteUser);

router.route('/search/users').post(controllers.Users.searchProfiles);


// POST ROUTES
// ----------------------------------------------------

router.route('/posts').post(controllers.Posts.listAll);

router.route('/posts/create').post(expressJwt({secret: config.secret}), controllers.Posts.create);

router.route('/posts/remove').put(expressJwt({secret: config.secret}), controllers.Posts.deleteMultiple);

router.route('/posts/user/:user_id').post(controllers.Posts.listByUserId);

router.route('/posts/:post_id')
    .get(controllers.Posts.byPostId)
    .put(expressJwt({secret: config.secret}), controllers.Posts.update)
    .delete(expressJwt({secret: config.secret}), controllers.Posts.delete);

router.route('/posts/search').post(controllers.Posts.searchPosts);

router.route('/posts/tags').post(controllers.Posts.listByTags);



// COMMENT ROUTES
// ----------------------------------------------------

// router.route('/comments')
// 	.post(controllers.PostComments.create);

router.route('/comments/post/:post_id')
	.get(controllers.PostComments.getCommentsForPost)
	.post(expressJwt({secret: config.secret}), controllers.PostComments.create)
	// .post(controllers.PostComments.commentsForPost);

router.route('/comments/user/:user_id')
	.get(controllers.PostComments.commentsForUser);

router.route('/comments/:comment_id')
	.get(controllers.PostComments.singleComment)
    .put(controllers.PostComments.update)
    .delete(controllers.PostComments.delete);



// EMOTION ROUTES
// ----------------------------------------------------

router.route('/emotions')
	.get(controllers.Emotions.listAll);

// All Feedback for a single post
router.route('/emotions/post/:post_id')
	.get(controllers.PostFeedback.getByPostId);

// Feedback on a single post related to a single user
router.route('/postfeedback/:post_id')
	.get(expressJwt({secret: config.secret}), controllers.PostFeedback.singlePostFeedbackForUser)
	.post(expressJwt({secret: config.secret}), controllers.PostFeedback.addRemovePostFeedback)


// Upload ROUTES
// ----------------------------------------------------

router.route('/upload/media')
	.post(controllers.Upload.media);


// MEDIA TYPE ROUTES
// ----------------------------------------------------

router.route('/media_types')
	.get(controllers.MediaTypes.listAll);





router.route('/*').get(function(req, res){
	return res.status(404).json({message: 'Not Found'});
});




module.exports = router;
