var path       = require('path');
var fs         = require('fs');

var controllers            = {};
var controllerDirArray     = fs.readdirSync('server/controllers');
var controllerRelativePath = path.relative( __dirname, process.cwd() + '/server/controllers' );

for(var i = 0; i < controllerDirArray.length; i++){
	var jsIndex = controllerDirArray[i].indexOf('.js');
	var variableName = controllerDirArray[i].substring(0, jsIndex);
	controllers[variableName] = require( './' + controllerRelativePath+ '/' + variableName );
}

module.exports = controllers;
