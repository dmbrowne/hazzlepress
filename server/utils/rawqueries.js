'use strict';

module.exports = {

	emotions: {
		getPostFeedbackWithCount: function(){
			return 'SELECT ' +
					'"PostFeedback"."emotion_id", ' +
					'COUNT("PostFeedback"."emotion_id") AS "emo_count", ' +
					'"EmotionsTable"."emotion_name" AS "emotion_name" ' +
				'FROM ' +
					'"post_feedback_emotions" AS "PostFeedback" ' +
				'LEFT JOIN ' +
					'(SELECT ' +
						'"emotion_name", "id" ' +
					 'FROM ' +
						'"emotions" ' +
					') "EmotionsTable" ' +
					  'ON  "PostFeedback"."emotion_id" = "EmotionsTable"."id" ' +
				'WHERE ' +
					'"PostFeedback"."post_id"= :postid ' +
				'GROUP BY ' +
					'"PostFeedback"."emotion_id", "EmotionsTable"."emotion_name"';
		}
	},

	posts: {
		getAllWithFeedbackAndCommentCount: function(options){
			var limitKey  = options.limitKey  || "limit";
			var offsetKey = options.offsetKey || "offset";

			return 'SELECT ' +
					'"Post"."id" AS "Post.id",  ' +
					'"Post"."description" AS "Post.description",  ' +
					'"Post"."created_at" AS "Post.created_at",  ' +
					'"Emotions"."emotion_name" AS "Emotions.name",  ' +
					'"PostEmotionThrough"."emotion_id" AS "Emotions.id",  ' +
					'"PostMedia"."id" AS "PostMedia.id",  ' +
					'"PostMedia"."location" AS "PostMedia.location",  ' +
					'"PostMedia.MediaType"."media_type" AS "PostMedia.media_type",  ' +
					'"User"."id" AS "User.id",  ' +
					'"User"."email" AS "User.email",  ' +
					'"User.Profile"."username" AS "User.Profile.username",  ' +
					'"User.Profile"."profile_photo" AS "User.Profile.profile_photo",  ' +
					'"User.Profile"."private" AS "User.Profile.private", ' +
					'"Feedback"."emotion_id" AS "Feedback.emotion_id", ' +
		    		'"Feedback"."count" AS "Feedback.emotion_count", ' +
		    		'"PostComments"."id" AS "PostComments.id", ' +
		    		'"PostComments"."comment" AS "PostComments.comment", ' +
		    		'"PostComments"."user_id" AS "PostComments.user_id", ' +
					'"CommentCount"."NumComments" AS "Post.TotalComments" ' +

				'FROM  ' +
					' (SELECT * FROM "posts" WHERE deleted_at is null ORDER BY created_at DESC LIMIT :'+limitKey+' OFFSET :'+offsetKey+' ) AS "Post"' +

				'LEFT JOIN ' +
					'"post_emotion_through" AS "PostEmotionThrough"' +
						 'ON   "Post"."id" = "PostEmotionThrough"."post_id"' +

				'LEFT JOIN ' +
					'"emotions" AS "Emotions"' +
						 'ON   "PostEmotionThrough"."emotion_id" = "Emotions"."id"' +

				'LEFT OUTER JOIN  ' +
					'"users" AS "User"  ' +
						 'ON     "Post"."user_id" = "User"."id"  ' +
								'AND "User"."deleted_at" IS NULL  ' +

				'LEFT OUTER JOIN  ' +
					'"profiles" AS "User.Profile"  ' +
						 'ON  "User"."id" = "User.Profile"."user_id"  ' +

				'LEFT OUTER JOIN  ' +
					'"post_media" AS "PostMedia"  ' +
						 'ON     "Post"."id" = "PostMedia"."post_id"  ' +

				'LEFT OUTER JOIN  ' +
					'"media_types" AS "PostMedia.MediaType"  ' +
						 'ON  "PostMedia"."media_type_id" = "PostMedia.MediaType"."id"  ' +

				'LEFT OUTER JOIN  ' +
					'( ' +
						'select * from (' +
							'select *, row_number() over (' +
								'partition by post_id ' +
								'ORDER by created_at desc' +
							') as row_num ' +
							'from post_comments ' +
						') as comments ' +
						'WHERE comments.row_num <= 3' +
					') ' +
					'"PostComments"  ' +
					'ON  "Post"."id" = "PostComments"."post_id" ' +

				'LEFT OUTER JOIN  ' +
					'(SELECT ' +
							'"post_id",  ' +
							'count(*) as "NumComments" ' +
					 'FROM  ' +
							'"post_comments" ' +
					'GROUP BY  ' +
							'"post_id" ' +
					') "CommentCount" ' +
						'ON      "Post"."id" = "CommentCount"."post_id" ' +

				'LEFT OUTER JOIN ' +
				    '(SELECT  ' +
				            'emotion_id,  ' +
				            'post_id, ' +
				            'count(emotion_id)  ' +
				     'FROM ' +
				            'post_feedback_emotions  ' +
				     'GROUP BY ' +
				            'post_id, emotion_id ' +
				    ') "Feedback" ' +
				        'ON "Post"."id" = "Feedback"."post_id" ' +

				'ORDER BY "Post"."created_at" DESC ';
		},

		getSingleWithFeedbackAndCommentCount: function(postidKey){
			return 'SELECT ' +
					'"Post"."id"                        AS "Post.id",                ' +
					'"Post"."description"               AS "Post.description",       ' +
					'"Post"."created_at"                AS "Post.created_at",        ' +
					'"Emotions"."emotion_name"          AS "Emotions.name",          ' +
					'"PostEmotionThrough"."emotion_id"  AS "Emotions.id",            ' +
					'"PostMedia"."id"                   AS "PostMedia.id",           ' +
					'"PostMedia"."location"             AS "PostMedia.location",     ' +
					'"PostMedia.MediaType"."media_type" AS "PostMedia.media_type",   ' +
					'"Feedback"."emotion_id"            AS "Feedback.emotion_id",    ' +
		    		'"Feedback"."count"                 AS "Feedback.emotion_count", ' +
					'"CommentCount"."NumComments"       AS "Post.TotalComments"      ' +

				'FROM  ' +
					' (SELECT * FROM "posts" WHERE (deleted_at is null AND id = :'+postidKey+' ) ) AS "Post"' +

				'LEFT JOIN ' +
					'"post_emotion_through" AS "PostEmotionThrough"' +
						 'ON   "Post"."id" = "PostEmotionThrough"."post_id"' +

				'LEFT JOIN ' +
					'"emotions" AS "Emotions"' +
						 'ON   "PostEmotionThrough"."emotion_id" = "Emotions"."id"' +

				'LEFT OUTER JOIN  ' +
					'"post_media" AS "PostMedia"  ' +
						 'ON     "Post"."id" = "PostMedia"."post_id"  ' +

				'LEFT OUTER JOIN  ' +
					'"media_types" AS "PostMedia.MediaType"  ' +
						 'ON  "PostMedia"."media_type_id" = "PostMedia.MediaType"."id"  ' +

				'LEFT OUTER JOIN  ' +
					'(SELECT ' +
							'"post_id",  ' +
							'count(*) as "NumComments" ' +
					 'FROM  ' +
							'"post_comments" ' +
					'GROUP BY  ' +
							'"post_id" ' +
					') "CommentCount" ' +
						'ON      "Post"."id" = "CommentCount"."post_id" ' +

				'LEFT OUTER JOIN ' +
				    '(SELECT  ' +
				            'emotion_id,  ' +
				            'post_id, ' +
				            'count(emotion_id)  ' +
				     'FROM ' +
				            'post_feedback_emotions  ' +
				     'GROUP BY ' +
				            'post_id, emotion_id ' +
				    ') "Feedback" ' +
				        'ON "Post"."id" = "Feedback"."post_id"';
		},

		getByUserIdWithFeedbackAndCommentCount: function(options){
			var limitKey  = options.limitKey;
			var offsetKey = options.offsetKey;
			var useridKey = options.useridKey;

			return 'SELECT ' +
					'"Post"."id"                        AS "Post.id",                ' +
					'"Post"."description"               AS "Post.description",       ' +
					'"Post"."created_at"                AS "Post.created_at",        ' +
					'"Emotions"."emotion_name"          AS "Emotions.name",          ' +
					'"PostEmotionThrough"."emotion_id"  AS "Emotions.id",            ' +
					'"PostMedia"."id"                   AS "PostMedia.id",           ' +
					'"PostMedia"."location"             AS "PostMedia.location",     ' +
					'"PostMedia.MediaType"."media_type" AS "PostMedia.media_type",   ' +
					'"Feedback"."emotion_id"            AS "Feedback.emotion_id",    ' +
		    		'"Feedback"."count"                 AS "Feedback.emotion_count", ' +
					'"CommentCount"."NumComments"       AS "Post.TotalComments"      ' +

				'FROM  ' +
					' (SELECT * FROM "posts" WHERE (deleted_at is null AND user_id = :'+useridKey+' ) LIMIT :'+limitKey+' OFFSET :'+offsetKey+' ) AS "Post"' +

				'LEFT JOIN ' +
					'"post_emotion_through" AS "PostEmotionThrough"' +
						 'ON   "Post"."id" = "PostEmotionThrough"."post_id"' +

				'LEFT JOIN ' +
					'"emotions" AS "Emotions"' +
						 'ON   "PostEmotionThrough"."emotion_id" = "Emotions"."id"' +

				'LEFT OUTER JOIN  ' +
					'"post_media" AS "PostMedia"  ' +
						 'ON     "Post"."id" = "PostMedia"."post_id"  ' +

				'LEFT OUTER JOIN  ' +
					'"media_types" AS "PostMedia.MediaType"  ' +
						 'ON  "PostMedia"."media_type_id" = "PostMedia.MediaType"."id"  ' +

				'LEFT OUTER JOIN  ' +
					'(SELECT ' +
							'"post_id",  ' +
							'count(*) as "NumComments" ' +
					 'FROM  ' +
							'"post_comments" ' +
					'GROUP BY  ' +
							'"post_id" ' +
					') "CommentCount" ' +
						'ON      "Post"."id" = "CommentCount"."post_id" ' +

				'LEFT OUTER JOIN ' +
				    '(SELECT  ' +
				            'emotion_id,  ' +
				            'post_id, ' +
				            'count(emotion_id)  ' +
				     'FROM ' +
				            'post_feedback_emotions  ' +
				     'GROUP BY ' +
				            'post_id, emotion_id ' +
				    ') "Feedback" ' +
				        'ON "Post"."id" = "Feedback"."post_id" ' +

				'ORDER BY "Post"."created_at" DESC ';
		},

		getByTags: function(options){
			var tagids    = options.tagids;
			var limitKey  = options.limitKey;
			var offsetKey = options.offsetKey;

			var newString = "(";
			tagids.forEach(function(tagid){
				newString = newString + '\''+ tagid + '\',';
			});

			var tagidSearch = newString.substr(0,(newString.length - 1)) + ')';

			return 'SELECT  ' +
				    '"Post"."id"                        AS "Post.id",  ' +
				    '"Post"."user_id"                   AS "Post.user_id",  ' +
				    '"Post"."description"               AS "Post.description",  ' +
				    '"Post"."created_at"                AS "Post.created_at",  ' +
				    '"Emotions"."emotion_name"          AS "Emotions.name",   ' +
				    '"PostEmotionThrough"."emotion_id"  AS "Emotions.id",   ' +
				    '"PostMedia"."id"                   AS "PostMedia.id",   ' +
				    '"PostMedia"."location"             AS "PostMedia.location",   ' +
				    '"PostMedia.MediaType"."media_type" AS "PostMedia.media_type",   ' +
				    '"User"."id"                        AS "User.id",   ' +
				    '"User"."email"                     AS "User.email",   ' +
				    '"User.Profile"."username"          AS "User.Profile.username",   ' +
				    '"User.Profile"."profile_photo"     AS "User.Profile.profile_photo",   ' +
				    '"User.Profile"."private"           AS "User.Profile.private",  ' +
				    '"Feedback"."emotion_id"            AS "Feedback.emotion_id",  ' +
				    '"Feedback"."count"                 AS "Feedback.emotion_count",  ' +
				    'COUNT("Post"."id")                 AS "Post.relevance", ' +
				    '"CommentCount"."NumComments"       AS "Post.TotalComments"  ' +
				'FROM ( ' +
				    'SELECT  ' +
				        '*  ' +
				    'FROM  ' +
				        'posts, post_tag_associations ' +
				    'WHERE  ' +
				        'id = post_id  ' +
				            'AND tag_id IN '+ tagidSearch +' ' +
				      'LIMIT :'+limitKey+' ' +
				      'OFFSET :'+offsetKey+
				   ') AS "Post" ' +

				'LEFT JOIN  ' +
				    '"post_emotion_through" AS "PostEmotionThrough" ' +
				         'ON   "Post"."id" = "PostEmotionThrough"."post_id" ' +

				'LEFT JOIN  ' +
				    '"emotions" AS "Emotions" ' +
				         'ON   "PostEmotionThrough"."emotion_id" = "Emotions"."id" ' +

				'LEFT OUTER JOIN   ' +
				    '"users" AS "User"   ' +
				         'ON     "Post"."user_id" = "User"."id"   ' +
				                'AND "User"."deleted_at" IS NULL  ' +

				'LEFT OUTER JOIN   ' +
				    '"profiles" AS "User.Profile"   ' +
				         'ON  "User"."id" = "User.Profile"."user_id"   ' +

				'LEFT OUTER JOIN   ' +
				    '"post_media" AS "PostMedia"   ' +
				         'ON     "Post"."id" = "PostMedia"."post_id"   ' +

				'LEFT OUTER JOIN   ' +
				    '"media_types" AS "PostMedia.MediaType"   ' +
				         'ON  "PostMedia"."media_type_id" = "PostMedia.MediaType"."id"   ' +

				'LEFT OUTER JOIN  ' +
				    '(SELECT  ' +
				            '"post_id",   ' +
				            'count(*) as "NumComments"  ' +
				     'FROM   ' +
				            '"post_comments"  ' +
				    'GROUP BY   ' +
				            '"post_id"  ' +
				    ') "CommentCount"  ' +
				        'ON      "Post"."id" = "CommentCount"."post_id"  ' +

				'LEFT OUTER JOIN  ' +
				    '(SELECT   ' +
				            'emotion_id,  ' +
				            'post_id,  ' +
				            'count(emotion_id) ' +
				     'FROM  ' +
				            'post_feedback_emotions  ' +
				     'GROUP BY  ' +
				            'post_id, emotion_id  ' +
				    ') "Feedback"  ' +
				        'ON "Post"."id" = "Feedback"."post_id" ' +

				'GROUP BY  ' +
				    '"Post"."id",  ' +
				    '"Post"."user_id",  ' +
				    '"Post"."description",  ' +
				    '"Post"."created_at",  ' +
				    '"Emotions"."emotion_name", ' +
				    '"PostEmotionThrough"."emotion_id", ' +
				    '"PostMedia"."id", ' +
				    '"PostMedia"."location", ' +
				    '"PostMedia.MediaType"."media_type", ' +
				    '"User"."id", ' +
				    '"User"."email", ' +
				    '"User.Profile"."username", ' +
				    '"User.Profile"."profile_photo", ' +
				    '"User.Profile"."private", ' +
				    '"Feedback"."emotion_id", ' +
				    '"Feedback"."count", ' +
				    '"CommentCount"."NumComments" ' +

				'ORDER BY ' +
				    '"Post.relevance" DESC, ' +
				    '"Post.created_at" DESC';
		},

		tsvSearch: function(options){
			var querykey  = options.queryKey;
			var limitKey  = options.limitKey;
			var offsetKey = options.offsetKey;

			return "SELECT *, ts_rank_cd(posts_tsv, q) AS rank \
				FROM posts, to_tsquery(:"+querykey+") AS q \
				WHERE (posts_tsv @@ q) \
				ORDER BY rank DESC \
				LIMIT :"+limitKey+" OFFSET :"+offsetKey;
		}
	}
};
