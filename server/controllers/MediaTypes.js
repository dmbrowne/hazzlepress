"use strict";

var models = require('../models');

exports.listAll = function(req, res){
	models.MediaType
		.findAll()
		.then(function(mediatypes){
			res.send(mediatypes);
		})
		.catch(function(err){
			console.error(err);
		})
}