"use strict";

var models = require('../models');

exports.listAll = function(req, res){
	models.Emotion
		.findAll()
		.then(function(emotions){
			res.send(emotions);
		})
		.catch(function(err){
			console.error(err);
			res.status(400).json({message: err});
		})
}
