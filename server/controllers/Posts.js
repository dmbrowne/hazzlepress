"use strict";

var models     = require('../models');
var rawQueries = require('../utils/rawqueries');
var $q         = require('q');
var _          = require('lodash');


exports.listAll = function(req, res){
	var limit  = req.body.per_page || 10;
	var offset = req.body.page_number ? limit * (req.body.page_number - 1) : 0;
	var q      = rawQueries.posts.getAllWithFeedbackAndCommentCount({
		limitKey: "limit",
		offsetKey: "offset"
	});

	models.sequelize.query(q, {
		replacements: {
			limit: limit,
			offset: offset
		}
	})
	.spread(function(posts, meta){
		var sequelizedPosts = models.Post.convertRawPostToPostObject(posts);
		res.status(200).json(sequelizedPosts);
	})
	.catch(function(err){
		console.error(err);
	});
}

exports.byPostId = function(req, res){
	if(!req.params.post_id)
		return res.status(400).json({message: "no post id received"});

	var postId = req.params.post_id;
	var q      = rawQueries.posts.getSingleWithFeedbackAndCommentCount("postid");

	models.sequelize.query(q, {
		replacements: { postid: req.params.post_id }
	})
	.spread(function(posts, meta){
		var sequelizedPosts = models.Post.convertRawPostToPostObject(posts);
		res.send(sequelizedPosts[0]);
	})
	.catch(function(err){
		res.status(400).json({message: err});
	});
}

exports.listByUserId = function(req, res){
	if(!req.params.user_id)
		return res.status(400).json({message: "no user id received"});

	var limit  = req.body.per_page || 10;
	var offset = req.body.page_number ? limit * (req.body.page_number - 1) : 0;
	var q      = rawQueries.posts.getByUserIdWithFeedbackAndCommentCount({
		useridKey: "userid",
		limitKey:  "limit",
		offsetKey: "offset"
	});

	models.sequelize.query(q, {
		replacements: {
			limit: limit,
			offset: offset,
			userid: req.params.user_id
		}
	})
	.spread(function(posts, meta){
		var sequelizedPosts = models.Post.convertRawPostToPostObject(posts);
		res.send(sequelizedPosts);
	})
	.catch(function(err){
		res.status(400).json({message: err});
	});
}

/* Currently not in use */
exports.listByUsername = function(req, res){
	if(!req.params.username)
		return res.status(400).json({message: "no username received"});

	var limit    = req.body.per_page || 10;
	var offset   = req.body.page_number ? limit * (req.body.page_number - 1) : 0;
	var username = req.body.username;
	var q        = rawQueries.posts.getByUserIdWithFeedbackAndCommentCount({
		useridKey: "userid",
		limitKey:  "limit",
		offsetKey: "offset"
	});

	sequelize.transaction().then(function ( t ){
		return models.Profile.find({
			attributes: ['id'],
			where: {username: username},
		},
		{
			transaction: t
		})
		.then(function(user){
			if(user.length < 1) throw new Error('user does not exist');

			return models.sequelize.query(q, {
				replacements: {
					limit:  limit,
					offset: offset,
					userid: user.getDataValue('id')
				}
			},
			{
				transaction: t
			})
			.spread(function(posts, meta){
				return posts;
			})
		})
	})
	.then(function(userposts){
		t.commit();
		var sequelizedPosts = models.Post.convertRawPostToPostObject(posts);
		res.send(sequelizedPosts);
	})
	.catch(function(err){
		t.rollback();
		res.status(400).json({message: err});
	});
}

exports.listByTags = function(req, res){
	if(!req.body.tags)
		return res.status(400).json({message: "Tags not found in request"});

	var limit = req.body.per_page || 10;
	var offset = req.body.page_number ? limit * (req.body.page_number - 1) : 0;
	var lowerCaseTags = req.body.tags.map(function(tagname){
		return tagname.toLowerCase()
	});

	models.sequelize.transaction().then(function (t){
		return models.Tag.findAll({
			attributes: ['id'],
			where: {name: lowerCaseTags}
		},{
			transaction: t
		})
		.then(function(tagsidObjs){

			if(tagsidObjs.length < 1){
				t.commit()
				return tagsidObjs
			}

			var tagids = _.pluck(tagsidObjs, 'id');
			var q      = rawQueries.posts.getByTags({
				tagids: tagids,
				limitKey:  "limit",
				offsetKey: "offset"
			});

			return models.sequelize.query(q, {
				replacements:{
					limit: limit,
					offset: offset
				},
				transaction: t
			})
			.spread(function(posts, meta){
				t.commit()
				return models.Post.convertRawPostToPostObject(posts)
			})
		})
	})
	.then(function(sequelizedPosts){
		return res.status(200).json(sequelizedPosts)
	})
	.catch(function(err){
		console.log(err)
		return res.status(400).json({message: err});
	});
}

exports.create = function(req, res){
	/* ---- Example of full post object -----
	{
		user_id: string
		description: string
		emotion_id: [integer, integer]
		media: ['base64String', 'base64String']
	}*/

	if(!req.user)
		return res.status(401).json({message: "user not found in token"})

	if(!req.body.user_id)
		return res.status(401).json({message: "user id not found request body"});

	if(req.user.id !== req.body.user_id)
		return res.status(401).json({message: "user id in request does not match user found in token"})

	var mediaArrayToInsert = req.body.media;
	var emotionIds         = req.body.emotion_id;

	models.sequelize.transaction().then(function ( transactionPromise ){
		return models.Post.create(req.body, {
			fields: models.Post.fillable,
			transaction: transactionPromise
		})
		.then(function(createdpost){
			var postEmotions = emotionIds.map(function(emotionId){
				return { emotion_id: emotionId, post_id: createdpost.id }
			})

			return models.PostEmotionThrough.bulkCreate(postEmotions,{
				transaction: transactionPromise
			})
			.then(function(){
				if(mediaArrayToInsert && mediaArrayToInsert.length){
					return models.Post.savePostMedia(
						createdpost.id,
						createdpost.user_id,
						mediaArrayToInsert,
						transactionPromise
					)
					.then(function(){ resolveTransaction(createdpost, transactionPromise) })
				}else{
					resolveTransaction(createdpost, transactionPromise)
				}
			})
		})
		.catch(function(err){ rejectTransaction(err, transactionPromise) })
	})

	function resolveTransaction(post, tPromise){
		return models.Post.find({
			where: {id: post.getDataValue('id')},
			include: [{
				model: models.Emotion
			},{
				model: models.PostMedia,
				include: [models.MediaType]
			}]
		}, {
			transaction: tPromise
		})
		.then(function(posts){
			tPromise.commit()
			return res.send(posts)
		}, function(err){
			tPromise.rollback()
			return res.status(400).json({message: err});
		})
	}

	function rejectTransaction(error, tPromise){
		tPromise.rollback()
		console.log(error);
		return res.status(400).json({message: error});
	}
}

exports.update = function(req, res){
	var updatePromises = [];

	if(!req.user)
		return res.status(401).json({message: "user not found in token"})

	if(!req.body.user_id)
		return res.status(401).json({message: "user id not found request body"});

	if(req.user.id !== req.body.user_id)
		return res.status(401).json({message: "user id in request does not match user found in token"});

	if(req.body.emotion_id && req.body.emotion_id.length)
		updatePromises.push( models.PostEmotionThrough.updatePostEmotions(req.body.emotion_id, req.params.post_id) );

	if(req.body.media && req.body.media.length)
		updatePromises.push( models.Post.savePostMedia( req.params.post_id, req.body.user_id, req.body.media ) );

	if(req.body.deleteMedia && req.body.deleteMedia.length > 0)
		updatePromises.push( removeMediasFromPost() );

	updatePromises.push(
		models.Post.update(req.body, {
			where: {
				id:      req.params.post_id,
				user_id: req.body.user_id
			},
			fields: ['description'],
			individualHooks: true
		})
	);

	$q.all(updatePromises).then(function(){
		models.Post.find({
			where: { id: req.params.post_id	},
			include: [
				models.Emotion,
				models.PostMedia
			]
		})
		.then(function(post){
			return res.send(post)
		},
		function(err){
			console.log('error ' + err)

		})
		.catch(function(err){
			return res.status(400).json({message: err});
		});
	})

	function removeMediasFromPost(){
		var deletePromises = [];

		_.forEach(req.body.deleteMedia, function(postMediaId){
			deletePromises.push(
				models.PostMedia.deleteMedia(postMediaId, {
					post_id: req.params.post_id
				})
			);
		});

		return $q.all(deletePromises);
	}
}

exports.delete = function(req, res){
	if(!req.user)
		return res.status(401).json({message: "user not found in token"})

	models.Post.destroy({
		where: {
			id:      req.params.post_id,
			user_id: req.user.id
		}
	})
	.then(
		function(){
			res.send();
		},
		function(err){
			return res.status(400).json({ message: err });
		}
	)
	.catch(function(err){
		return res.status(400).json({ message: err });
	})
}

exports.deleteMultiple = function(req, res){
	if(!req.user)
		return res.status(401).json({message: "user not found in token"})

	models.Post.destroy({
		where: {
			id: req.body.post_ids,
			user_id: req.user.id
		}
	})
	.then(
		function(rowsAffected){
			res.status(200).json({rows: rowsAffected});
		},
		function(err){
			return res.status(400).json({ message: err });
		}
	)
	.catch(function(err){
		return res.status(400).json({ message: err });
	})
}

exports.searchPosts = function(req, res){
	var searchArray = req.body.search_terms;
	var queryString = searchArray.join(' & ');
		queryString = queryString + ':*';
	var limit       = req.body.per_page || 15;
	var offset      = req.body.page_number ? limit * (req.body.page_number - 1) : 0;
	var q           = rawQueries.posts.tsvSearch({
		queryKey: "query",
		limitKey: "limit",
		offsetKey: "offset"
	});

	models.sequelize.query(q, {
		replacements: {
			query: queryString,
			limit: limit,
			offset: offset
		}
	})
	.spread(function(posts, meta){
		return res.send(posts);
	})
	.catch(function(err){
		console.error(err);
		return res.status(400).json({
			message: err
		});
	})
}
