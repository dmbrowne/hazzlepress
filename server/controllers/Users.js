"use strict";

var models = require('../models');
var $q     = require('q');
var env    = process.env.NODE_ENV || "development";
var config = require(__dirname + '/../config/config.json')[env];
var jwt    = require('jsonwebtoken');
var _      = require('lodash');

exports.listAll = function(req, res){
	models.User
		.findAll({
			include: [ models.Profile ]
		})
		.then(function(users){
			users.forEach(function(instance){
				// instance.removeSensitiveValues();
			})
			res.send(users);
		})
		.catch(function(err){
			console.error(err);
		})
}

exports.getToken = function(req, res){
	var mailOrUser, requestPassword, getUserBy;

	if(req.body.email){
		mailOrUser = req.body.email;
		getUserBy = 'user';
	}else if(req.body.username){
		mailOrUser = req.body.username;
		getUserBy = 'profile';
	}else{
		return res.status(401).json({ message: "provide a username or email to log in with" });
	}

	if(!req.body.password)
		return res.status(401).json({ message: "provide a password to log in with" });

	requestPassword = req.body.password;

	models.User.getUserByUsernameOrProfile(mailOrUser, getUserBy)
		.then(function(user){
			if( user === null )
				return res.status(400).json({ message: "user doesn't exist" });

			user.comparePassword(requestPassword, function(err, isMatch){
				if(err) return res.status(500).json({ message: "error comparing password" });
				if(isMatch){
					var userSafeJson = user.removeSensitiveValues();
					var token = jwt.sign(userSafeJson, config.secret, { expiresInMinutes: 60*5 });
					res.json({ token: token });
				}else{
					return res.status(401).json({ message: "incorrect password" });
				}
			});
		});
}

exports.create = function(req, res){
	var submittedEmail, submittedpassword;

	if(req.body.email && req.body.password){
		submittedEmail = req.body.email;
		submittedpassword = req.body.password;
	}else{
		return res.status(400).json({
			message: 'Did not receive either, or neither email and password'
		});
	}

	models.User
		.findOrCreate({
			where: { email: submittedEmail},
			defaults: { password: submittedpassword}
		})
		.spread(function(user, created) {
			if(created === false)
				return res.status(200).json(user);
			else
				return res.status(201).json(user);
		})
		.catch(function(err){
			return res.status(400).json({
				message: err
			});
		});
}

exports.getUser = function(req, res){
	var userIdOrName = req.params.user_id_name;

	models.sequelize.transaction().then(function ( t ){
		return models.User.find({
			where: {id: userIdOrName},
			include: [models.Profile]
		},{
			transaction: t
		})
		.then(function(user){
			if(user !== null){
				t.commit()
				return res.send(user)
			}

			return models.User.find({
				include: {
					where: {username : userIdOrName},
					model: models.Profile,
					as: 'Profile'
				}
			},{
				transaction: t
			})
			.then(function(user){
				t.commit()
				return res.send(user)
			})
		})
		.catch(function(err){
			t.rollback();
			console.error(err);
			return res.status(400).json({ message: err });
		})
	})
}

exports.getUserById = function(req, res){
	var userId = req.params.user_id;
	models.User.find({
		where: {id: userId},
		include: [models.Profile]
	})
	.then(function(user){
		return res.send(user);
	})
	.catch(function(err){
		console.error(err);
		return res.status(400).json({
			message: err
		});
	})
}

exports.getUserByUsername = function(req, res){

	if(!req.params.username)
		return res.status(400).json({message: "username not provided"});

	models.User.find({
		include: {
			where: {username : req.params.username},
			model: models.Profile,
			as: 'Profile'
		}
	})
	.then(function(user){
		return res.send(user);
	})
	.catch(function(err){
		console.error(err);
		return res.status(400).json({
			message: err
		});
	})
}

exports.getSessionUser = function(req, res){
    var sessionUser = req.user;

    if(sessionUser === undefined){
        return res.status(401).json({message: "token not recieved"});
    }

	models.User
		.find({
			where: {id: sessionUser.id},
			include: [models.Profile]
		})
		.then(function(user){
			return res.send(user);
		})
		.catch(function(err){
			console.error(err);
			return res.status(400).json({
				message: err
			});
		})
}

/* refactor: REMOVE */
/*
exports.getCurrentUser = function(req, res){
    var sessionUser = req.user;
	var userId = req.params.user_id;

    if(userId != sessionUser.id){
        return res.status(401).json({message: "requested user is not equal to supplied  token"});
    }

	models.User
		.find({
			where: {id: userId},
			include: [models.Profile]
		})
		.then(function(user){
			return res.send(user);
		})
		.catch(function(err){
			console.error(err);
			return res.status(400).json({
				message: err
			});
		})
}
*/

exports.updateUser = function(req, res){
	var userId        = req.params.user_id;
	var promises      = [];
	var newUserValues = {};

	if(req.body.email)
		newUserValues.email = req.body.email;

	if(req.body.password)
		newUserValues.password = req.body.password;

	if( _.size(newUserValues) > 0 ){
		promises.push(models.User.update(newUserValues,{
			where: {id: userId}
		}));
	}

	if(req.body.profile){
		promises.push(updateProfile());
	}

	$q.all(promises).then(function(){
		models.User.find({
			where: {id: userId},
			include: [models.Profile]
		})
		.then(function(updatedUser){
			return res.send(updatedUser);
		});
	})
	.catch(function(err){
		console.error(err);
		return res.status(400).json({ message: err });
	});

	function updateProfile(){
		var deferred = $q.defer();
		var avatarUpdate = null;

		if(req.body.profile.profile_photo){
			avatarUpdate = models.PostMedia.saveMediaToDisk({
				mediatype: 'avatar',
				base64String: req.body.profile.profile_photo[0]
			});
		}

		$q.when(avatarUpdate, function(returnedMedia){
			if(returnedMedia !== null)
				req.body.profile.profile_photo = returnedMedia.filename;

			updateProfileTable(req.body.profile).then(function(){
				deferred.resolve();
			})
		});

		return deferred.promise;
	}

	function updateProfileTable(profileobj){
		var updateUserProfile;
		if(profileobj.id){
			updateUserProfile = models.Profile.update(profileobj, { where: {id: profileobj.id} })
		}else{
			updateUserProfile = models.Profile.findOrCreate({
				where: { user_id: userId },
				defaults: profileobj
			}).spread(function(user,created){
				if(!created)
					return models.Profile.update(profileobj, { where: {user_id: userId} })
				else{
					return user;
				}
			});
		}
		return updateUserProfile;
	}
}

exports.deleteUser = function(req, res){
    if(!req.user)
        return res.status(401).json({message: "user not found in token"})

    if(req.user.id !== req.body.user_id)
        return res.status(401).json({message: "user id in request does not match user found in token"});

	var userId = req.params.user_id;

	models.User.destroy({
		where: {id: userId}
	}).
	then(function(){
		res.send("user deleted");
	})
	.catch(function(err){
		return res.status(400).json({
			message: err
		});
	})
}

exports.searchProfiles = function(req, res){
	var searchArray = req.body.search_terms;
	var queryString = searchArray.join(' & ');
		queryString = queryString + ':*';
	var limit       = req.body.per_page || 15;
	var offset      = req.body.page_number ? limit * (req.body.page_number - 1) : 0;

	models.sequelize.query(
		"SELECT *, ts_rank_cd(profiles_tsv, q) AS rank \
		FROM profiles, to_tsquery(:query) AS q \
		WHERE (profiles_tsv @@ q) \
		ORDER BY rank DESC \
		LIMIT :limit OFFSET :offset", null,
		{raw: true},
		{
			query: queryString,
			limit: limit,
			offset: offset
		}
	)
	.then(function(profiles){
		return res.send(profiles);
	})
	.catch(function(err){
		console.error(err);
		return res.status(400).json({
			message: err
		});
	})
}
