"use strict"

var models = require('../models');

exports.getByPostId = function(req, res){
	var postid = req.params.post_id;

	models.sequelize.query(
		'SELECT ' +
			'"PostFeedback"."emotion_id", ' +
			'COUNT("PostFeedback"."emotion_id") AS "emo_count", ' +
			'"EmotionsTable"."emotion_name" AS "emotion_name" ' +
		'FROM ' +
			'"post_feedback_emotions" AS "PostFeedback" ' +
		'LEFT JOIN ' +
			'(SELECT ' +
				'"emotion_name", "id" ' +
			 'FROM ' +
				'"emotions" ' +
			') "EmotionsTable" ' +
			  'ON  "PostFeedback"."emotion_id" = "EmotionsTable"."id" ' +
		'WHERE ' +
			'"PostFeedback"."post_id"= :postid ' +
		'GROUP BY ' +
			'"PostFeedback"."emotion_id", "EmotionsTable"."emotion_name"'
		, {
			replacements: { postid: postid }
		}
	)
	.spread(
		function(emotionCount, meta){
			res.send(emotionCount);
		},
		function(err){
			res.status(400).json({message: err});
		}
	)
	.catch(function(err){
		console.error(err);
	})
}

exports.addRemovePostFeedback = function(req, res){
	if(!req.user)
		res.status(401).json({message: 'please provide a user token'})

	if(!req.body.emotion_ids || req.body.emotion_ids.length < 1)
			res.status(400).json({message: 'provide emotion_ids as an array or integers'})


	models.sequelize.transaction().then(function ( tChain ){
		return models.sequelize.Promise.map(req.body.emotion_ids, function(emotionid){
			return models.PostFeedback.findOrCreate({
				where: {
					emotion_id: emotionid,
					user_id: req.user.id,
					post_id: req.params.post_id
				},
				defaults: {
					emotion_id: emotionid,
					user_id: req.user.id,
					post_id: req.params.post_id
				}
			},{
				transaction: tChain
			})
			.spread(function(userPostFeedback, created){
				if(created)
					return tChain
				else
					return userPostFeedback.destroy({transaction: tChain})
			})
		},{
			transaction: tChain
		})
		.then(function(){
			tChain.commit()
			return models.Emotion.findAll({
				include:{
					attributes: [],
					model: models.PostFeedback,
					where: {
						user_id: req.user.id,
						post_id: req.params.post_id
					}
				}
			})
			.then(function(userFeedBack){
				return userFeedBack
			})
		})
		.catch(function(err){
			tChain.rollback()
			return err
		})
	})
	.then(
		function(userFeedBack, meta){
			res.send(userFeedBack)
		},
		function(err){
			res.status(500).json({message: err})
		}
	)
}

// For displaying what a user has previously selected on a post
// e.g. liked, loved, hated
exports.singlePostFeedbackForUser = function(req, res){
	var userid = req.user.id;
	var postid = req.params.post_id;

	models.Emotion.findAll({
		include:{
			attributes: [],
			model: models.PostFeedback,
			where: {
				'post_id': postid,
				'user_id': userid
			}
		}
	})
	.then(
		function(feedback){
			return res.send(feedback);
		},
		function(err){
			return res.status(400).json({message: err});
		}
	)
	.catch(function(err){
		return res.status(400).json({message: err});
	})
}
