var fs = require('fs');

exports.media = function (req, res) {
	var fileUploadStream;
	var saveDir = __dirname + '/../storage/media';

    req.busboy.on('file', function (fieldname, file, filename) {

        fileUploadStream = fs.createWriteStream(saveDir + '/' + filename);

        file.pipe(fileUploadStream);

        fileUploadStream.on('close', function () {
        	res.send('media/' + filename)
        });
    });
};
