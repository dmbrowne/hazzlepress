"use strict"

var models = require('../models');


exports.singleComment = function(req, res){
	var commentId = req.params.comment_id;

	models.PostComment
		.find({ where:{id: commentId }}, {raw: true})
		.then(function(comment){
			return res.send(comment);
		})
		.catch(function(err){
			return res.status(400).json({message: err});
		});
}

exports.getCommentsForPost = function (req, res) {
	var postId = req.params.post_id;
	var limit  = req.query.per_page || 15;
	var offset = req.query.page_number ? limit * (req.query.page_number - 1) : 0;

	models.PostComment
		.findAll({
			attributes: ['comment', 'created_at', 'updated_at'],
			where:{post_id: postId},
			limit: limit,
			offset: offset,
			order: 'created_at DESC',
			include: {
				model: models.User,
				attributes: ['id'],
				include: {
					model: models.Profile,
					attributes: ['username', 'profile_photo'],
				}
			}
		})
		.then(function(comments){
			return res.send(comments);
		})
		.error(function(err){
			return res.status(400).json({message: err});
		})
		.catch(function(err){
			return res.status(400).json({message: err});
		});
}

exports.commentsForUser = function (req, res) {
	var userId = req.params.user_id;
	var limit  = req.query.per_page || 15;
	var offset = req.query.page_number ? limit * (req.query.page_number - 1) : 0;

	models.PostComment
		.findAll({
			where:{user_id: userId},
			limit: limit,
			offset: offset,
			order: 'created_at DESC'
		})
		.then(
			function(comments){
				return res.send(comments);
			}, function(err){
				return res.status(400).json({message: err});
			}
		)
		.catch(function(err){
			return res.status(400).json({message: err});
		});
}


//  Create
exports.create = function(req, res){
	var fillableFields = models.PostComment.fillable();

	if(!req.user)
		return res.status(401).json({message: "user not found in token"})

	// if(!req.body.user_id)
	// 	return res.status(401).json({message: "user id not found request body"});

	// if(req.user.id !== req.body.user_id)
	// 	return res.status(401).json({message: "user id in request does not match user found in token"});

	models.PostComment
		.create({
			post_id: req.params.post_id,
			user_id: req.user.id,
			comment: req.body.comment
		},
		{
			fields: fillableFields
		})
		.then(function(createdComment){
			return res.send(createdComment)
		})
		.catch(function(err){
			return res.status(400).json({message: err});
		})
}


// Update
exports.update = function(req, res){
	var commentId = req.params.comment_id;

	if(!req.user)
		return res.status(401).json({message: "user not found in token"})

	// if(!req.body.user_id)
	// 	return res.status(401).json({message: "user id not found request body"});

	// if(req.user.id !== req.body.user_id)
	// 	return res.status(401).json({message: "user id in request does not match user found in token"});

	models.PostComment
		.find({ where:{id: commentId }})
		.then(function(recievedComment){
			recievedComment.updateAttributes({
				comment: req.body.comment
			})
			.then(function(updatedComment){
				return res.send(updatedComment)
			})
		})
		.catch(function(err){
			return res.status(400).json({message: err});
		});
}


// Delete
exports.delete = function(req, res){
	if(!req.user)
		return res.status(401).json({message: "user not found in token"})

	if(!req.body.user_id)
		return res.status(401).json({message: "user id not found request body"});

	if(req.user.id !== req.body.user_id)
		return res.status(401).json({message: "user id in request does not match user found in token"});

	models.Post
		.find({where: {id: req.body.post_id}})
		.then(deletePost)
		.catch(function(err){
			return res.status(400).json({message: err})
		});

	function deleteComment(comment){
		if(req.body.user_id !== comment.user_id)
			return res.status(401).json({message: "user_id does not match the user_id of the comment" })

		comment.destroy().then(function(){
			res.send("comment deleted");
		});
	};
}
