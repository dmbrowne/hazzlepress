"use strict";

var $q = require("q");
var fs = require("fs");
var chance = require('chance').Chance();

module.exports = function(sequelize, DataTypes) {
	var PostMedia = sequelize.define("PostMedia", {

		location: {
			type: DataTypes.STRING,
			allowNull: false
		},

		media_type_id:{
			type: DataTypes.INTEGER,
			allowNull: false,
			references: 'media_types',
			referencesKey: 'id'
		},

		post_id: {
			type: DataTypes.STRING(24)
		}
	},
	{
		// add the timestamp attributes (updatedAt, createdAt)
		timestamps: true,

		deletedAt: false,
		updatedAt: false,

		// don't delete database entries but set the newly added attribute deletedAt
		// to the current date (when deletion was done). paranoid will only work if
		// timestamps are enabled
		paranoid: false,

		// use camelcase for automatically added attributes or underscore style
		// so updatedAt will be updated_at, and createdAt will be created_at
		underscored: true,

		// disable the modification of tablenames:
		// By default, sequelize will automatically transform all passed model
		// names (first parameter of define) into plural. eg 'User' becomes 'Users'.
		// if you don't want that, set the following
		// freezeTableName: true,

		// define the table's name, default plural/singular name based on defined model name
		// above in sequelize.define("ModelName")
		tableName: 'post_media',

		classMethods: {
			associate: function(models) {
				PostMedia.belongsTo(models.MediaType, {foreignKey: 'media_type_id'});
				PostMedia.belongsTo(models.Post);
			},

			decodeBase64Image: function(dataString) {
				var matches  = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
				var response = {};

				if (matches.length !== 3)
					return new Error('Invalid input string');

				response.type = matches[1];
				response.data = new Buffer(matches[2], 'base64');

				return response;

				/*
				 * var imageBuffer = decodeBase64Image(data);
				 * 	console.log(imageBuffer);
				 * 		{ type: 'image/jpeg',
				 * 		  data: <Buffer 89 50 4e 47 0d 0a 1a 0a 00 00 00 ...> }
				 * fs.writeFile('test.jpg', imageBuffer.data, function(err) { ... });
				 *
				 */
			},

			saveMediaToDisk: function(mediaObject){
				var deferred        = $q.defer();
				var imageBuffer     = PostMedia.decodeBase64Image(mediaObject.base64String);
				var postid          = mediaObject.postid || "";
				var mediatype       = mediaObject.mediatype || "post";
				var user_id         = mediaObject.user_id || chance.natural({min: 100000});

				var extension       = undefined;
				var datetime        = Date.parse(Date());
				var mediaFileType   = imageBuffer.type.substring(0, imageBuffer.type.indexOf('/'));

				if(imageBuffer.type === 'image/jpeg') extension = '.jpg';
				if(imageBuffer.type === 'image/png') extension = '.png';

				var uniqueFileName = datetime + postid + user_id + extension;
				var saveDir        = (mediatype === 'avatar' ? '/../storage/avatars/large/' : '/../storage/media/');
				var fullSaveDir    = __dirname + saveDir + uniqueFileName;

				fs.writeFile( fullSaveDir, imageBuffer.data, function(err){
					if(err) deferred.reject(err);

					deferred.resolve({ filename: uniqueFileName, mediaFileType: mediaFileType });
				});

				return deferred.promise;
			},

			addMediaToDB: function(filename, postid, mediatypeName, tPromise){
				return sequelize.transaction(function ( t ){
					t = tPromise || t;
					return sequelize.model('MediaType').find(
							{
								attributes: ['id'],
								where: { media_type: mediatypeName }
							},
							{
								transaction: t
							}
						).then(function(media_type){
								return PostMedia.create(
									{
										location: filename,
										post_id: postid,
										media_type_id: media_type.getDataValue('id')
									},
									{
										transaction: t
									}
								)
							},
							function(err){
								console.log(err)
								throw new Error(err);
							}
						);
				})
			},

			deleteMedia: function(postMediaId, options){
				var deferred = $q.defer();

				var mediaKind = options.mediaKind || 'post'; // 'post' or 'avatar'
				var postWhereConditions = { id: postMediaId };

				if(options.post_id)
					postWhereConditions.post_id = options.post_id;

				PostMedia.find({ where: postWhereConditions })
					.then(
						function(postmedia){
							if(postmedia == null || postmedia.length <= 0)
								deferred.reject("media with select where condition doesn't exist");

							var mediaLocationDir = (
								mediaKind === 'avatar' ?
									'/../storage/avatars/large/' : '/../storage/media/'
							);
							var filelocation = mediaLocationDir + postmedia.location;

							PostMedia.deleteMediaFile(filelocation)
								.then(function(){
									postmedia.destroy({force: true}).then(deferred.resolve, deferred.reject);

								}, function(err){
									if(err.code === 'ENOENT')
										postmedia.destroy({force: true}).then(deferred.resolve, deferred.reject); // File doesnt exist/ already deleted, so remove it from the media table
									else
										deferred.reject(err);
								});
						},

						function(err)
						{
							deferred.reject(err)
						}
					);

				return deferred.promise;
			},

			deleteMediaFile: function(filelocation){
				var deferred = $q.defer();

				fs.unlink(filelocation, function (err) {
					if(err) deferred.reject(err);
					deferred.resolve();
				});

				return deferred.promise;
			},


		}
	});

	return PostMedia;
};
