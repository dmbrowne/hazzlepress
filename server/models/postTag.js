"use strict";

module.exports = function(sequelize, DataTypes) {
	var postTag = sequelize.define("PostTagRelation", {

		tag_id: {
			type: DataTypes.STRING(24),
			references: "tags",
			referencesKey: "id",
			allowNull: false
		},

		post_id: {
			type: DataTypes.STRING(24),
			references: "posts",
			referencesKey: "id",
			allowNull: false
		}
	},
	{
		indexes: [{
			unique: true,
			fields: ['tag_id', 'post_id']
		}],

		// add the timestamp attributes (updatedAt, createdAt)
		timestamps: false,

		// don't delete database entries but set the newly added attribute deletedAt
		// to the current date (when deletion was done). paranoid will only work if
		// timestamps are enabled
		// paranoid: true,

		// use camelcase for automatically added attributes or underscore style
		// so updatedAt will be updated_at, and createdAt will be created_at
		// underscored: true,

		// disable the modification of tablenames; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural. eg 'User' becomes 'Users'
		// if you don't want that, set the following
		freezeTableName: true,

		// define the table's name, default plural/singular name based on defined model name
		// above in sequelize.define("ModelName"
		tableName: 'post_tag_associations',


		classMethods: {
			associate: function(models) {
				// associations can be defined here
			}
		}
	});

	return postTag;
};
