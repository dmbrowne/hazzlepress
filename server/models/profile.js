"use strict";

module.exports = function(sequelize, DataTypes) {
	var Profile = sequelize.define("Profile", {

		user_id: {
			type: DataTypes.STRING(24),
			allowNull: false
		},

		username: {
			type: DataTypes.STRING(20),
			allowNull: true
		},

		name: DataTypes.STRING(35),

		bio: DataTypes.STRING(160),

		profile_photo: DataTypes.TEXT,

		website: DataTypes.STRING(60),

		phone: DataTypes.STRING(20),

		gender: DataTypes.ENUM('male', 'female'),

		private: DataTypes.BOOLEAN
	},
	{
		// add the timestamp attributes (updatedAt, createdAt)
		timestamps: true,

		deletedAt: false,

		// don't delete database entries but set the newly added attribute deletedAt
		// to the current date (when deletion was done). paranoid will only work if
		// timestamps are enabled
		paranoid: false,

		// use camelcase for automatically added attributes or underscore style
		// so updatedAt will be updated_at, and createdAt will be created_at
		// underscored: true,

		// disable the modification of tablenames; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural. eg 'User' becomes 'Users'
		// if you don't want that, set the following
		// freezeTableName: false,

		// define the table's name, default plural/singular name based on defined model name
		// above in sequelize.define("ModelName"
		tableName: 'profiles',


		classMethods: {
			associate: function(models) {
				// associations can be defined here
				Profile.belongsTo(models.User);
			}
		}
	});

	return Profile;
};