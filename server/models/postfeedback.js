"use strict";

var $q = require("q");
var _  = require('lodash');

module.exports = function(sequelize, DataTypes) {
	var PostFeedback = sequelize.define("PostFeedback", {

		post_id:{
			type: DataTypes.STRING(24),
			allowNull: false
		},

		user_id:{
			type: DataTypes.STRING(24),
			allowNull: false
		},

		emotion_id: {
			type: DataTypes.INTEGER,
			allowNull: false
		}

	},
	{
		// add the timestamp attributes (updatedAt, createdAt)
		// timestamps: false,

		// don't delete database entries but set the newly added attribute deletedAt
		// to the current date (when deletion was done). paranoid will only work if
		// timestamps are enabled
		paranoid: false,

		// use camelcase for automatically added attributes or underscore style
		// so updatedAt will be updated_at, and createdAt will be created_at
		underscored: true,

		// disable the modification of tablenames; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural. eg 'User' becomes 'Users'
		// if you don't want that, set the following
		freezeTableName: true,

		// define the table's name, default plural/singular name based on defined model name
		// above in sequelize.define("ModelName"
		tableName: 'post_feedback_emotions',

		classMethods: {
			associate: function(models) {
				// associations
				PostFeedback.belongsTo(models.User);
				PostFeedback.belongsTo(models.Post);
				PostFeedback.belongsTo(models.Emotion);
			},

			fillable: [
				'post_id',
				'user_id',
				'emotion_id'
			]
		}
	});

	return PostFeedback;
};
