"use strict";

var $q = require("q");
var _  = require('lodash');

module.exports = function(sequelize, DataTypes) {
	var Post = sequelize.define("Post",{
		user_id:{
			type: DataTypes.STRING(24),
			allowNull: false
		},

		description: DataTypes.STRING(500)
	}, {
		// timestamps: false,
		// paranoid: true,
		underscored: true,
		tableName: 'posts',

		classMethods: {
			fillable: ['user_id','description'],
			associate: associations,
			savePostMedia: savePostMedia,
			returnTagsFromString: getHashTagsFromString,
			convertRawPostToPostObject: convertRawPostToPostObject
		}
	});

	Post.hook('afterCreate', afterCreateHook);
	Post.hook('beforeUpdate', beforeUpdateHook);
	Post.hook('afterDestroy', afterDestroyHook);

	return Post;

	function getHashTagsFromString(str){
		var hashTags = str.match(/#\S+/g);

		if(hashTags === null || hashTags.length < 1) return [];

		var tags = hashTags.map(function(tag){
			return tag.substr(1).toLowerCase();
		});

		return tags;
	}

	function associations(models) {
		Post.belongsTo(models.User);
		Post.belongsToMany(models.Tag, {through: models.PostTagRelation, foreignKey: 'post_id'});
		Post.belongsToMany(models.Emotion, {through: models.PostEmotionThrough, foreignKey: 'post_id'});
		Post.hasMany(models.PostFeedback);
		Post.hasMany(models.PostComment);
		Post.hasMany(models.PostMedia);
	}

	function savePostMedia(postid, userid, mediaSourceBase64Array, tPromise){
		var deferred = $q.defer();
		var newPostMediaRows = [];
		var transaction = tPromise || undefined;

		for(var i = 0; i < mediaSourceBase64Array.length; i++){
			var base64String = mediaSourceBase64Array[i];

			sequelize.model('PostMedia').saveMediaToDisk({
				postid: postid,
				user_id: userid,
				base64String: base64String
			})
			.then(function(savedFile){
				sequelize.model('PostMedia')
					.addMediaToDB(savedFile.filename, postid, savedFile.mediaFileType, transaction)
					.then(function(newPostMediaRow){
						newPostMediaRows.push(newPostMediaRow);

						if( (i+1) >= mediaSourceBase64Array.length )
							deferred.resolve(newPostMediaRows);
					});
			});
		};

		return deferred.promise;
	}

	function afterCreateHook(createdPost){
		var postHashtags = Post.returnTagsFromString(createdPost.description);

		if(postHashtags.length < 1 ) return sequelize.Promise.resolve(createdPost);

		sequelize.transaction().then( function ( t ) {
			return sequelize.Promise.map(postHashtags, function(hashtag){
				return sequelize.model('Tag').findOrCreate({
					where: {name: hashtag},
					defaults: {name: hashtag}
				},{
					transaction: t
				})
				.spread(function(tag, created){
					return sequelize.model('PostTagRelation').create({
						tag_id: tag.getDataValue('id'),
						post_id: createdPost.getDataValue('id')
					},{
						transaction: t
					});
				})
			}).then(function(hashtags){
				t.commit();
				return sequelize.Promise.resolve(createdPost);
			}).catch(function(err){
				console.log(err);
				t.rollback();
				return sequelize.Promise.reject(err);
			})
		});
	}

	function beforeUpdateHook(postToBeUpdated){
		if( !postToBeUpdated.changed('description') ) return sequelize.Promise.resolve(postToBeUpdated);

		var modificationPromises = [];
		var previousTags         = Post.returnTagsFromString(postToBeUpdated._previousDataValues.description);
		var tagsInUpdatedPost    = Post.returnTagsFromString(postToBeUpdated.dataValues.description);
		var tagsToRemove         = _.difference(previousTags, tagsInUpdatedPost);
		var newTags              = _.difference(tagsInUpdatedPost, previousTags);
		var unchangedTags        = _.intersection(previousTags, tagsInUpdatedPost);

		modificationPromises.push(
			sequelize.transaction(function ( t ){
				return sequelize.model('Tag').findAll({
					attributes: ['id'],
					where: {name: tagsToRemove}
				},{
					transaction: t
				})
				.then(function(tagsToRemoveObjects){
					var tagIdsToRemove = _.pluck(tagsToRemoveObjects, 'id');

					return sequelize.model('PostTagRelation').destroy({
						where:{
							tag_id: tagIdsToRemove,
							post_id: postToBeUpdated.dataValues.id
						}
					},{ transaction: t });
				})
			})
		);

		modificationPromises.push(
			sequelize.transaction(function ( t ){
				return sequelize.Promise.map(newTags, function(hashtag){
					return sequelize.model('Tag').findOrCreate({
						where: {name: hashtag},
						defaults: {name: hashtag}
					},{
						transaction: t
					})
					.spread(function(tag, created){
						return sequelize.model('PostTagRelation').create({
							tag_id: tag.getDataValue('id'),
							post_id: postToBeUpdated.dataValues.id
						},{ transaction: t });
					})
				});
			})
		);

		$q.all(modificationPromises).then(function(){
			return sequelize.Promise.resolve(postToBeUpdated);
		}).catch(function(err){
			console.log(err);
			return sequelize.Promise.reject(err);
		});
	}

	function afterDestroyHook(deletedPost){
		sequelize.model('PostTagRelation').destroy({
			where:{ post_id: deletedPost.dataValues.id }
		}).then(function(){
			return sequelize.Promise.resolve(deletedPost);
		}).catch(function(err){
			console.log(err);
			return sequelize.Promise.reject(err);
		})
	}

	function convertRawPostToPostObject(rawposts){
		var uniquePosts = _.uniq(rawposts, 'Post.id');

		/* loop thru each post e.g.
		 *
		 [
			{ <--- Loop this

				id: 324232
				user: 345353

			},
			{ <--- Loop this

				id: 324232
				user: 345353

			}
		 ]
		 */
		rawposts.forEach(function(rawPostObj, idx){
			var user            = {};
			var profile         = {};
			var emotion         = {};
			var comment         = {};
			var feedback        = {};
			var media           = {};
			var referrencedPost = _.filter(uniquePosts, {'Post.id': rawPostObj['Post.id']} )[0];

			if(!referrencedPost.processedValues){
				referrencedPost.processedValues = {}
			}

			/* loop thru each key value in post e.g.
			 *
			 [
				{
					id: 324232    <--- Loop this
					user: 345353  <--- Loop this
				},
				{
					id: 324232    <--- Loop this
					user: 345353  <--- Loop this
				}
			 ]
			 */
			_.forOwn(rawPostObj, function(value, key){

				if(key.indexOf("Emotions.") >= 0){
					var keyname = key.substr(key.indexOf(".") + 1);
					if(value !== null) emotion[keyname] = value
				}

				if(key.indexOf("Feedback.") >= 0){
					var keyname = key.substr(key.indexOf(".") + 1);
					if(value !== null) feedback[keyname] = value
				}

				if(key.indexOf("PostMedia.") >= 0){
					var keyname = key.substr(key.indexOf(".") + 1);
					if(value !== null) media[keyname] = value
				}

				if(key.indexOf("PostComments.") >= 0){
					var keyname = key.substr(key.indexOf(".") + 1);
					if(value !== null) comment[keyname] = value
				}

				if(key.indexOf("Post.") >= 0){
					var keyname = key.substr(key.indexOf(".") + 1);
					referrencedPost.processedValues[keyname] = value
				}

				//extract userModelKeys
				if(key.indexOf("User.") >= 0 && key.indexOf(".") === key.lastIndexOf(".")){
					var keyname = key.substr(key.indexOf(".") + 1);
					user[keyname] = value
				}

				//extract userModelKeys
				if(key.indexOf(".Profile.") >= 0){
					var keyname = key.substr(key.lastIndexOf(".") + 1);
					profile[keyname] = value
				}

			});

			if(_.size(user) > 0)
				referrencedPost.processedValues.User = user;

			if(_.size(profile) > 0)
				referrencedPost.processedValues.User.Profile = profile;

			createAndUpdateManytoMany(emotion, 'Emotions');
			createAndUpdateManytoMany(media, 'Media');
			createAndUpdateManytoMany(comment, 'Comments');
			createAndUpdateManytoMany(feedback, 'Feedback', 'emotion_id');

			function createAndUpdateManytoMany(dataObj, holdingArray, properyToCheck){
				properyToCheck = properyToCheck || 'id';

				if(_.size(dataObj) > 0){
					var exists = [];

					if(referrencedPost.processedValues[holdingArray])
						var exists = _.filter(referrencedPost.processedValues[holdingArray], { 'id' : dataObj[properyToCheck] });
					else
						referrencedPost.processedValues[holdingArray] = [];

					if(exists.length === 0)
						referrencedPost.processedValues[holdingArray].push(dataObj);
				}
			}

			rawposts[idx] = referrencedPost
		});

		var postObjectsWithoutRaw = _.pluck(uniquePosts, 'processedValues');
		return postObjectsWithoutRaw;
	}
};
