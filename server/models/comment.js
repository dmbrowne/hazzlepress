"use strict";

module.exports = function(sequelize, DataTypes) {
	var PostComment = sequelize.define("PostComment", {

		post_id:{
			type: DataTypes.STRING(24),
			allowNull: false,
			references: "posts",
			referencesKey: "id"
		},

		comment: DataTypes.STRING(500),

		user_id: {
			type: DataTypes.STRING(24),
			references: "users",
			referencesKey: "id"
		},

		created_at: {
			type: DataTypes.DATE,
			allowNull: false,
		},

		updated_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: DataTypes.NOW
		},

		deleted_at: DataTypes.DATE

	},
	{
		// disable the modification of tablenames; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural. eg 'User' becomes 'Users'
		// if you don't want that, set the following
		freezeTableName: true,

		// define the table's name, default plural/singular name based on defined model name
		// above in sequelize.define("ModelName"
		tableName: 'post_comments',


		classMethods: {
			associate: function(models) {
				PostComment.belongsTo(models.Post);
				PostComment.belongsTo(models.User);
			}
		}
	});

	return PostComment;
};
