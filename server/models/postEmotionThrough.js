"use strict";

var $q = require("q");
var _  = require('lodash');

module.exports = function(sequelize, DataTypes) {
	var PostEmotionThrough = sequelize.define("PostEmotionThrough", {

		post_id:{
			type: DataTypes.STRING(24),
			allowNull: false
		},

		emotion_id: {
			type: DataTypes.INTEGER,
			allowNull: false
		}

	},
	{
		// add the timestamp attributes (updatedAt, createdAt)
		timestamps: false,

		// don't delete database entries but set the newly added attribute deletedAt
		// to the current date (when deletion was done). paranoid will only work if
		// timestamps are enabled
		// paranoid: true,

		// use camelcase for automatically added attributes or underscore style
		// so updatedAt will be updated_at, and createdAt will be created_at
		underscored: true,

		// disable the modification of tablenames; By default, sequelize will automatically
		// transform all passed model names (first parameter of define) into plural. eg 'User' becomes 'Users'
		// if you don't want that, set the following
		freezeTableName: true,

		// define the table's name, default plural/singular name based on defined model name
		// above in sequelize.define("ModelName"
		tableName: 'post_emotion_through',

		classMethods: {
			associate: function(models) {
				// associations
			},

			fillable: function(){
				return [
					'post_id',
					'emotion_id'
				];
			},

			updatePostEmotions: function(emotionIds, post_id){
				PostEmotionThrough.findAll({ where: { post_id: post_id } })
					.then(function(currentEmotions){
						var currentEmotionIds = _.pluck(currentEmotions, 'emotion_id');
						var idsToRemove       = _.difference(currentEmotionIds, emotionIds);
						var newIds            = _.difference(emotionIds, currentEmotionIds);
						var unchangedIds      = _.intersection(currentEmotionIds, emotionIds);

						var deleteInsertPromises = idsToRemove.map(function(id){
							return PostEmotionThrough.destroy({
								where: {
									emotion_id: id,
									post_id: post_id
								}
							});
						});

						var idsToInsert = newIds.map(function(id){
							return {
								emotion_id: id,
								post_id: post_id
							};
						});

						deleteInsertPromises.push(PostEmotionThrough.bulkCreate(idsToInsert));

						return $q.all(deleteInsertPromises);
					})
			}
		}
	});

	return PostEmotionThrough;
};
