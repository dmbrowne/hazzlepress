"use strict";

var bcrypt = require('bcrypt');
var $q     = require('q');

module.exports = function(sequelize, DataTypes) {
	var User = sequelize.define("User", {

		email: {
			type: DataTypes.STRING(50),
			allowNull: false,
			validate:{
				notEmpty: true
			}
		},

		password: {
			type: DataTypes.STRING,
			allowNull: false,
			validate:{
				notEmpty: true
			}
		},

		active: {
			type: DataTypes.BOOLEAN,
			defaultValue: true,
			allowNull: false
		}

	},
	{
		// add the timestamp attributes (updatedAt, createdAt)
		timestamps: true,

		// don't delete database entries but set the newly added attribute deletedAt
		// to the current date (when deletion was done). paranoid will only work if
		// timestamps are enabled
		// paranoid: true,

		// use camelcase for automatically added attributes or underscore style
		// so updatedAt will be updated_at, and createdAt will be created_at
		underscored: true,

		// disable the modification of tablenames:
		// By default, sequelize will automatically transform all passed model
		// names (first parameter of define) into plural. eg 'User' becomes 'Users'.
		// if you don't want that, set the following
		// freezeTableName: true,

		// define the table's name, default plural/singular name based on defined model name
		// above in sequelize.define("ModelName")
		tableName: 'users',

		classMethods: {
			associate: function(models) {
				User.hasOne(models.Profile);
				User.hasMany(models.Post);
				User.hasMany(models.PostComment);
				User.belongsToMany(models.PostFeedback, {through: 'post_feedback_emotions', foreignKey: 'user_id'});
			},

			generateHashedPassword: function(unhashedPW, cb){
				bcrypt.hash(unhashedPW, bcrypt.genSaltSync(8), cb);
			},

			getUserByUsernameOrProfile: function(nameOrEmail, getBy){
				if (getBy = 'user')
					{
						return sequelize.model('User').find({
							where: {email: nameOrEmail},
							include: [sequelize.model('Profile')]
						});
					}
				else if (getBy = 'profile')
					{
						return sequelize.transaction(function ( t ){
							return sequelize.model('Profile').find({
								where: {username: nameOrEmail}
							}, {
								transaction: t
							})
							.then(function(profile){
								return sequelize.model('User').find({
									where: {id: profile.getDataValue('user_id')},
									include: [sequelize.model('Profile')]
								}, {
									transaction: t
								});
							})
						});
					}
			}
		},

		instanceMethods: {
			comparePassword : function(candidatePassword, cb) {
				var savedHash = this.getDataValue('password').replace(/\\/g, '');

				bcrypt.compare(candidatePassword, savedHash, function(err, isMatch) {
					if(err) return cb(err);
					cb(null, isMatch);
				});
			},

			removeSensitiveValues: function(){
				delete this.dataValues.password;
				return JSON.stringify(this.dataValues);
			}
		},

		hooks: {
			beforeCreate: function(user, options, cb){
				User.generateHashedPassword(user.password, function(err, hashedPassword){
					if(err) return cb(err);
					user.password = hashedPassword;
					cb(null, user);
				});
			},

			beforeUpdate: function(user, options, cb){
				//user.changed()      // will return an array of the attributes changed.
				//user.changed(key)   // will return an boolean for whether or not a specific attribute has changed.

				if(user.changed(password)){
					User.generateHashedPassword(user.password, function(err, hashedPassword){
						if(err) return cb(err);
						user.password = hashedPassword;
						cb(null, user);
					});
				}
			}
		}
	});

	return User;
};
