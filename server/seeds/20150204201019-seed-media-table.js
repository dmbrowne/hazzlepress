"use strict";
var _       = require('lodash');
var models  = require('../models');
var fs      = require('fs');

module.exports = {
	up: function(migration, DataTypes, done) {
		var mediaFiles = getMediaDirectoryFiles();
		var mediaInsertArray = [];

		models.MediaType.findAll().then(function(mediatypes){
			var videoMediaTypeId = _.pluck( _.filter(mediatypes, {'media_type': 'video'}), 'id');
			var imageMediaTypeId = _.pluck( _.filter(mediatypes, {'media_type': 'image'}), 'id');

			_.forEach(mediaFiles.imgs, function(imageMediaFileName){
				mediaInsertArray.push({
					location: imageMediaFileName,
					media_type_id: imageMediaTypeId[0]
				});
			});

			_.forEach(mediaFiles.videos, function(videoMediaFileName){
				mediaInsertArray.push({
					location: videoMediaFileName,
					media_type_id: videoMediaTypeId[0]
				});
			});

			models.PostMedia.bulkCreate(mediaInsertArray)
				.then(function(){ done(); })
				.catch(console.error);
		})

		function getMediaDirectoryFiles(){
			var returnFiles = {};
			var files       = fs.readdirSync(__dirname + '/../storage/media');
			var videos      = [];
			var imgs        = [];

			for(var i=0; i < files.length; i++){
				var extension = files[i].substr(files[i].lastIndexOf(".") + 1);

				if(extension === 'mp4')
					videos.push(files[i]);

				if(extension === 'jpeg' || extension === 'jpg' || extension === 'png')
					imgs.push(files[i]);
			}

			returnFiles.videos = videos;
			returnFiles.imgs   = imgs;

			return returnFiles;
		}
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('truncate table post_media cascade')
		done();
	}
};
