"use strict";

// Load Chance and Instantiate Chance so it can be used
var chance = require('chance').Chance();
// Get Models
var models  = require('../models');

var fs   = require('fs');
var path = require('path');
var $q   = require('q');
var _    = require('lodash');

module.exports = {
	up: function(migration, DataTypes, done) {
		var rowsToMake = 50;

		$q([
			models.Emotion.findAll(),
			models.User.findAll()
		])
		.spread(bulkCreatePosts)
		.catch(function(err){
			console.log(err)
			done(err)
		});

		function bulkCreatePosts(emotions, users){
			var emotionIds      = _.pluck(emotions, 'id');
			var userids         = _.pluck(users, 'id');
			var nonHashtagPosts = buildNormalPosts();
			var hashtagPosts    = buildHashtagPosts();
			var posts           = nonHashtagPosts.concat(hashtagPosts);

			for (var i = 0; i < posts.length; i++) {
				posts[i].user_id = chance.pick(userids);
			}

			models.Post.bulkCreate(posts)
				.then(function(){
					done()
				})
				.catch(function(error){
					console.log("inserting posts ERROR is \n", error);
					done(error);
				});

			function buildNormalPosts(){
				var descriptionOnlyPosts = [];

				for(var i=0; i < rowsToMake; i++){
					descriptionOnlyPosts[i] = createPostWithoutHashTags();
				};

				return descriptionOnlyPosts;
			}

			function buildHashtagPosts(){
				var hashtagPosts  = [];

				for(var i=0; i < rowsToMake; i++){
					hashtagPosts[i] = createPostWithHashtags();
				}

				return hashtagPosts;
			}

			function createPostWithoutHashTags() {
				return {
					user_id: undefined,
					description: chance.sentence({
						words: chance.natural({min: 5, max: 12})
					}),
					emotion_id: chance.pick(emotionIds)
				};
			}

			function createPostWithHashtags() {
				var numberOfTagsToGenerate = chance.natural({min: 1, max: 6});
				var tags                   = [];
				for (var i = 0; i < numberOfTagsToGenerate; i++) {
					tags[i] = chance.hashtag();
				};

				var joinedTags        = tags.join(" ");
				var normalSentence    = chance.sentence({words: chance.natural({min: 5, max: 12})});
				var tagsAndNormal     = joinedTags + " " + normalSentence;
				var sentenceTagsArray = chance.shuffle( tagsAndNormal.split(" ") );
				var finalSentence     = sentenceTagsArray.join(" ");

				return {
					user_id: undefined,
					description: finalSentence,
					emotion_id: chance.pick(emotionIds)
				};
			}
		}
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('TRUNCATE TABLE posts CASCADE')
			.then(function(){
				console.log('truncated');
				done();
			});
	}
};
