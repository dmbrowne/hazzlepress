"use strict";

// Get Models
var models  = require('../models');

module.exports = {
	up: function(migration, DataTypes, done) {
		// http://www.materialpalette.com/
		var emotions = [
			{ emotion_name: "LOL",  is_voteable: true }, // yellow & yellow
			{ emotion_name: "WTF",  is_voteable: true }, // purple and amber
			{ emotion_name: "Love", is_voteable: true }, // pink & red
			{ emotion_name: "Like", is_voteable: true }, // light-blue & indigo
			{ emotion_name: "Hate", is_voteable: true }, // brown & grey
			{ emotion_name: "Joy/Happy" },               // green & lime
			{ emotion_name: "Mellow/ Indifferent" },     // deep purple or amber & yellow
			{ emotion_name: "Sad" },                     // blue grey & brown
			{ emotion_name: "Disgust" },                 // grey & brown
			{ emotion_name: "Anger" }                    // red & brown
		];

		models.Emotion.bulkCreate(emotions)
			.then(function(){
				done();
			})
			.catch(console.log);
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('TRUNCATE TABLE emotions CASCADE')
			.then(function(){
				console.log('truncated emotions \n');
				done();
			});
	}
};
