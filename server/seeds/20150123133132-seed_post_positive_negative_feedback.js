"use strict";

// Load Chance and Instantiate Chance so it can be used
var chance  = require('chance').Chance();
var models  = require('../models');
var fs      = require('fs');
var _  = require('lodash');
var $q      = require('q');


module.exports = {
  up: function(migration, DataTypes, done) {
  	var user_ids = [];
  	var post_ids = [];

	$q.all([
		models.User.findAll({ attributes: ['id'] }),
		models.Post.findAll({ attributes: ['id', 'user_id'] }, {raw: true} ),
		models.Emotion.findAll({ 
			attributes: ['id'], 
			where:{
				is_voteable: true
			}
		})
	])
	.spread(function(users, postIdsAndUsers, emotions){
		var emotionIds       = _.pluck(emotions, 'id');
		var postAndUserids   = _.pick(postIdsAndUsers[0], ['id', 'user_id']);
		var userids          = _.pluck(users, 'id');
		var bulkInsertArray  = [];

		postIdsAndUsers.forEach(function(postmodel){
			var clonedUserids           = userids.slice(0);
			var numberOfFeedbacksToGive = chance.natural({min: 0, max: 18});
			var feedback                = undefined;
			var feedbackUserId          = undefined;
			var idx                     = 0;

			for(idx = 0; idx < numberOfFeedbacksToGive; idx++){
				feedback       = userFromRemainingSet(clonedUserids);
				feedbackUserId = feedback.userid;
				clonedUserids  = feedback.newarray;

				if(feedbackUserId === postmodel.user_id){
					feedback       = userFromRemainingSet(clonedUserids);
					feedbackUserId = feedback.userid;
					clonedUserids  = feedback.newarray;
				}

				bulkInsertArray.push({
					post_id: postmodel.id,
					user_id: feedbackUserId,
					emotion_id: chance.pick(emotionIds)
				});
			}
		});

		models.PostFeedback
		.bulkCreate(bulkInsertArray)
		.then(function(){
			done()
		})
		.error(function(err){
			console.error(err)
		})
		.catch(function(err){
			console.error(err)
		});
	})

	function userFromRemainingSet(userset){
		var userid = chance.pick(userset);
		var idxPos = userset.indexOf(userid);
		userset.splice(idxPos, 1);
		var returnObj = { userid: userid, newarray: userset}
		return returnObj;
	}
  },

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    migration.sequelize.query('truncate table post_feedback_emotions cascade')
    .then(function(){
    	done();
    });
  }
};
