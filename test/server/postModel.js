'use strict';

var expect = require('chai').expect;
var assert = require('chai').assert;
var base64 = require('../base64string.js')()
var PostModel = require('../../server/models').Post;

describe('Post model', function(){

	describe('convert raw to object class method', function(){
		it('should return nested object from raw post object', function(){
			var samplePosts = [
				{
					"Post.id":                    "54f46141f0e6b42ca628001d",
					"Post.description":           "test description",
					"Post.created_at":            "2015-03-02T13:10:25.420Z",
					"Post.TotalComments":         38,
					"User.id":                    "54e1e07d7df8300b62cee23a",
					"User.email":                 "daryl.browne@googlemail.com",
					"User.Profile.username":      "dbrowne88",
					"User.Profile.profile_photo": null,
					"User.Profile.private":       false,
					"Emotions.id":                 4,
					"Emotions.name":               "like"
				},
				{
					"Post.id":                     "54f46141f0e6b42ca628001d",
					"Post.description":            "test description",
					"Post.created_at":             "2015-03-02T13:10:25.420Z",
					"TotalComments":               38,
					"User.id":                     "54e1e07d7df8300b62cee23a",
					"User.email":                  "daryl.browne@googlemail.com",
					"User.Profile.username":       "dbrowne88",
					"User.Profile.profile_photo":  null,
					"User.Profile.private":        false,
					"Emotions.id":                  2,
					"Emotions.name":                "WTF"
				}
			];

			var convertedObj = PostModel.convertRawPostToPostObject(samplePosts);

			expect(convertedObj[0].id).to.be.equal("54f46141f0e6b42ca628001d");
			expect(convertedObj[0].description).to.be.equal("test description");
			expect(convertedObj[0].created_at).to.be.equal("2015-03-02T13:10:25.420Z");
			expect(convertedObj[0].TotalComments).to.equal(38);

			assert.isObject(convertedObj[0].User);
			expect(convertedObj[0].User.id).to.be.equal("54e1e07d7df8300b62cee23a");
			expect(convertedObj[0].User.email).to.be.equal("daryl.browne@googlemail.com");

			expect(convertedObj[0].User.Profile.username).to.be.equal("dbrowne88");
			assert.isNull(convertedObj[0].User.Profile.profile_photo);
			assert.isFalse(convertedObj[0].User.Profile.private);

			assert.isArray(convertedObj[0].Emotions);
			assert.isObject(convertedObj[0].Emotions[0]);
			expect(convertedObj[0].Emotions[0].id).to.be.equal(4);
			expect(convertedObj[0].Emotions[1].id).to.be.equal(2);
		})
	})

	describe('#Post class method "returnTagsFromString"', function(){
		it('should return array with correct tags and correct number of tags', function(){
			var stringWithTags = "if your feeling #froggy then leap! #murdaaa #dontSayaWord";
			var tagsArray = PostModel.returnTagsFromString(stringWithTags);

			expect(tagsArray.length).to.equal(3);
			expect(tagsArray[0]).to.equal("froggy");
			expect(tagsArray[1]).to.equal("murdaaa");
			expect(tagsArray[2]).to.equal("dontsayaword");
		})
	})

})
