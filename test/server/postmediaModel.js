var expect = require('chai').expect;
var assert = require('chai').assert;
var base64 = require('../base64string.js')()
var PostMediaModel = require('../../server/models').PostMedia;

describe('PostMedia model', function(){

	describe('#class method decodeBase64Image', function(){
		it("should return an object with a properties: type and data", function(done){
			var imageBuffer = PostMediaModel.decodeBase64Image(base64);
			assert.isString(imageBuffer.type);
			expect(imageBuffer.data).to.not.be.undefined;
			done();
		})
	})

})

