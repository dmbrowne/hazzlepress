process.env.NODE_ENV = 'test';

var sinon   = require('sinon');
var expect  = require('chai').expect;
var assert  = require('chai').assert;
var request = require('supertest');
var app     = require(__dirname + '/../../app');

describe('Emotions API Route Controller', function(){

	describe('#get Post Feedback', function(){

		it('should return array of feedback count from raw query', function(done){
			request(app)
				.get('/api/v1/emotions/post/54dd0aae581f6417ec0d3bc4')
				.type('json')
				.end(function(err, res){
					if(err) done(err);
					expect(res.status).to.equal(200);
					assert.isArray(res.body);
					done();
				})
		})
	})
})
