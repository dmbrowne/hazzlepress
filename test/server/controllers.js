var expect      = require('chai').expect;
var controllers = require(__dirname + '/../../server/routes/routeControllers');
var _           = require('lodash');
var fs          = require('fs');

describe('Route Controllers Object', function(){

	it('should produce object with same number of properties that exist in controllers dir', function(){
		var controllerDirArray = fs.readdirSync('server/controllers')
		expect(_.size(controllers)).to.equal(controllerDirArray.length);
	})
});
