process.env.NODE_ENV = 'test';

'use strict';

var $q      = require('q');
var expect  = require('chai').expect;
var assert  = require('chai').assert;
var request = require('supertest');
var models  = require('../../server/models');
var app     = require(__dirname + '/../../app');
var base64  = require('../base64string.js')()

var newUserDetails = {
	email: "test@mochatest.com",
	password: "mochatest"
};

describe('API User routes', function(){

	describe('#POST /users', function(){
		describe('create a user', function(){
			after(forceDeleteTestUser);

			describe('brand new', function(){
				before(forceDeleteTestUser);
				after(forceDeleteTestUser);

				it('should return 201 for brand new created user', function(done){
					request(app)
						.post('/api/v1/users')
						.send(newUserDetails)
						.end(function(err, res){
							if(err) done(err);

							expect(res.status).to.equal(201);
							expect(res.body.email).to.equal("test@mochatest.com");
							done();
						})
				});
			})

			describe('previously deleted with paranoia', function(){
				before(function(){
					return models.User.create(newUserDetails);
				});

				after(forceDeleteTestUser);

				it('should return 200 for restored user', function(done){
					request(app)
						.post('/api/v1/users')
						.send(newUserDetails)
						.end(function(err, res){
							if(err) done(err);

							expect(res.status).to.equal(200);
							expect(res.body.email).to.equal("test@mochatest.com");
							done();
						})
				});
			})
		})
	})

	describe('#POST /login', function(){
		beforeEach(function(){ return models.User.create(newUserDetails) });
		after(forceDeleteTestUser);

		it('should return token', function(done){
			request(app)
				.post('/api/v1/login')
				.send(newUserDetails)
				.end(function(err, res){
					if(err) done(err);

					expect(res.status).to.equal(200);
					expect(res.body.token).to.not.be.undefined;
					done();
				})
		});
	})

	describe('#GET /users/current', function(){
		var token;

		after(forceDeleteTestUser);
		before(function(done){
			createUserAndReturnToken().then(function(userToken){
				token = userToken;
				done();
			})
		});

		it('should return token', function(done){
			request(app)
				.get('/api/v1/users/current')
				.set('Authorization', 'Bearer ' + token)
				.end(function(err, res){
					if(err) done(err);
					expect(res.status).to.equal(200);
					expect(res.body.email).to.equal(newUserDetails.email);
					done();
				})
		});
	})

	describe('#GET /users/:user_id_name', function(){
		var userid;
		before(function(done){
			return models.User.create(newUserDetails).then(function(user){
				userid = user.getDataValue('id');
				done();
			});
		});
		after(forceDeleteTestUser);

		it('should return user', function(done){
			request(app)
				.get('/api/v1/users/'+userid)
				.end(function(err, res){
					if(err) done(err);
					expect(res.status).to.equal(200);
					expect(res.body.email).to.equal(newUserDetails.email);
					done();
				})
		});
	})

	describe('#PUT /users/:user_id', function(){
		var userid;

		before(function(done){
			return models.User.create(newUserDetails).then(function(user){
				userid = user.getDataValue('id');
				done();
			});
		});

		after(function(done){
			models.Profile.destroy({
				where: {username: "mochaMan"}
			}).then(function(){
				models.User.destroy({
					where: { id: userid },
					force: true
				}).then(function(){
					done();
				})
			})
		});

		it('should return user', function(done){
			request(app)
				.put('/api/v1/users/'+userid)
				.send({
					email: "mocha2@foobar.com",
					profile:{
						profile_photo: [base64],
						username: "mochaMan"
					}
				})
				.end(function(err, res){
					if(err) done(err);
					expect(res.status).to.equal(200);
					expect(res.body.email).to.equal("mocha2@foobar.com");
					expect(res.body.Profile.profile_photo).not.to.be.null;
					expect(res.body.Profile.username).to.equal("mochaMan");
					done();
				})
		});
	})

	describe('#DELETE /user/:user_id', function(){
		var userid, userToken;

		before(function(done){
			createUserAndReturnToken().then(function(token, userId){
				userid = userId;
				userToken = token;
				done();
			})
		})

		after(function(){
			return models.User.destroy({
				where: { id: userid },
				force: true
			})
		})

		it('should delete a user', function(done){
			request(app)
				.delete('/api/v1/users/'+userid)
				.set('Authorization', 'Bearer '+ userToken)
				.end(function(err, res){
					if(err) done(err);
					models.User.find({where: {id: userid} }).then(function(user){
						expect(user).to.be.null;
						done();
					})
				})
		})
	})
})

function forceDeleteTestUser(){
	return models.User.destroy({
		where: { email: "test@mochatest.com" },
		force: true
	});
}

function createUserAndReturnToken(){
	var deferred = $q.defer();

	models.User.create(newUserDetails).then(function(user){
		request(app)
			.post('/api/v1/login')
			.send(newUserDetails)
			.end(function(err, res){
				if(err) done(err);
				deferred.resolve(res.body.token, user.getDataValue('id'));
			})
	});

	return deferred.promise;
}
