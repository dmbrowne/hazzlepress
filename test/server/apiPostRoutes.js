process.env.NODE_ENV = 'test';

'use strict';

var sinon   = require('sinon');
var $q      = require('q');
var expect  = require('chai').expect;
var assert  = require('chai').assert;
var request = require('supertest');
var models  = require('../../server/models');
var app     = require(__dirname + '/../../app');
var base64  = require('../base64string.js')()

var userID, userToken;
var newUserDetails = {
	email: "test@mochatest.com",
	password: "mochatest"
};

describe('API Post routes', function () {

	before(function(done){
		models.User.create(newUserDetails).then(function(user){
			userID    = user.getDataValue('id')
			userToken = jwt.sign(user, config.secret, { expiresInMinutes: 60*5 })
			done()
		})
	})

	after(function(){
		return models.User.destroy({
			where: {id: userID },
			force: true
		})
	})
})
